# Definition of an initial state to understand antarctica’s tipping point

The understanding of the tipping point of Antarctica is the goal of the European Horizon 2020 research project called TiPACCs. It investigating the possibility of sudden and large changes in Antarctic Climate Components. Concurrently, recent research indicates that ice sheets, especially the parts of the ice sheet that rest on a bed below sea level, are prone to an unstable and irreversible retreat. If irreversible changes occur in the Antarctic components, and so-called tipping points are crossed, the ice sheet will likely quickly retreat, causing a dramatic increase in the global mean sea level. In the project, different models are used, we will focus on the part modeled with Elmer/Ice.
Elmer/Ice is a finite element model used with the Shallow Shelf approximation on the Antarctica ice-sheet. 

Before, applying any perturbation to an ice-sheet to observe the tipping point, it’s mandatory to define an initial state. To run an initial state, we define the topography of the ice-sheet and the parametrization of the forcing on the ice-sheet. The friction law used is also important as done by Brondex (2019), we are able to apply a non-linear friction law as the shoof law. This initial simulation needs to be relaxed to be close to reaching a steady-state. 

We tested the new topography of the bedrock provided by Bedmachine
[Morlighem, 2019] and we compare the roughness to his predecessor,
Bedmap2 [Fretwell, 2013]. We ran simulation with the both bedrock and
ice-thickness provided by these data. We observe the variation due to
the modification of the topography. An inversion using the data of
bedmachine has been running and compare to the bedmap2 inversion and simulation.
