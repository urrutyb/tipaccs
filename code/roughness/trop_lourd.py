import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.interpolate import griddata
from matplotlib.colors import SymLogNorm
from matplotlib import cm
import geopandas as gpd
import seaborn as sns
from matplotlib.colors import LogNorm
from scipy import ndimage
from mpl_toolkits.axes_grid1 import make_axes_locatable
import glob

size=40
params = {'legend.fontsize': 'xx-large',
          'figure.titlesize':'xx-large',
          'figure.figsize': (20,8),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.rcParams.update(params)


def stat(x):
    mean=np.nanmean(x)
    median=np.nanquantile(x,0.5)
    quartile1=np.nanquantile(x,0.25)
    quartile3=np.nanquantile(x,0.75)
    std=np.nanstd(x)
    return mean,median,quartile1,quartile3,std

def figure(x,y,z,sf,titlevariable,colorbartitle,param):
    stati=stat(z)
    fig=plt.figure(figsize=[20,12])
    plt.rcParams.update({'font.size': 15})
    ax=plt.gca()
    sf.plot(ax=ax, color='k')
    im=plt.pcolormesh(x,y,z,norm=SymLogNorm(linthresh=param[0], linscale=param[1] , vmin=param[2], vmax=param[3]),cmap=cm.seismic)
    cbar=plt.colorbar(im)
    plt.xlim(x.min(),x.max())
    plt.ylim(y.min(),y.max())
    cbar.set_label(colorbartitle)
    plt.title(titlevariable + 'Bedmap2-bedmachine \n  mean=' + str(stati[0]) + ' median=' +str(stati[1]) + ' quartile 25%=' +str(stati[2]) +'\n quartile 75%=' +str(stati[3]) + ' std=' +str(stati[4]))
    #fig.savefig(titlevariable + '.png')
    plt.show()
    #plt.close('all')
    #fig1=plt.figure(figsize=[20,12])
    #plt.rcParams.update({'font.size': 15})
    #sns.boxplot(z)
    #plt.close('all')

def line_flow(flowline,x,y,z_bed1,z_bed2):
    #interpolate from line
    x_fl,y_fl=np.loadtxt(flowline + '.csv',skiprows=1,delimiter=',',usecols=(22,23),unpack=True) 
    x_temp=(x_fl-x.min())/1000
    y_temp=(y_fl-y.min())/1000
    #bedmachine resolution 500m
    x_tmp=(x_fl-x.min())/500
    y_tmp=(y_fl-y.min())/500   
    # profil along line interpolated
    f_bed1 = ndimage.map_coordinates(np.array(z_bed1),[y_temp,x_temp],mode='nearest', order=1)
    f_bed2 = ndimage.map_coordinates(np.array(z_bed2),[y_tmp,x_tmp],mode='nearest', order=1)
    dist=np.zeros(len(x_fl))
    for i in range(0,len(x_fl)-1):
        dist[i+1]=dist[i]+np.sqrt((x_fl[i]-x_fl[i+1])**2+(y_fl[i]-y_fl[i+1])**2)/1000
    return x_fl,y_fl,f_bed1,f_bed2,dist

# DISCLAIMER: This function is copied from https://github.com/nwhitehead/swmixer/blob/master/swmixer.py, 
#             which was released under LGPL. 
def resample_by_interpolation(signal, input_fs, output_fs):

    scale = output_fs / input_fs
    # calculate new length of sample
    n = round(len(signal) * scale)

    # use linear interpolation
    # endpoint keyword means than linspace doesn't go all the way to 1.0
    # If it did, there are some off-by-one errors
    # e.g. scale=2.0, [1,2,3] should go to [1,1.5,2,2.5,3,3]
    # but with endpoint=True, we get [1,1.4,1.8,2.2,2.6,3]
    # Both are OK, but since resampling will often involve
    # exact ratios (i.e. for 44100 to 22050 or vice versa)
    # using endpoint=False gets less noise in the resampled sound
    resampled_signal = np.interp(
        np.linspace(0.0, 1.0, n, endpoint=False),  # where to interpret
        np.linspace(0.0, 1.0, len(signal), endpoint=False),  # known positions
        signal,  # known data points
    )
    return resampled_signal

# chargement des données
sf = gpd.read_file(
    "/home/urrutyb/Documents/PhD_TiPACCS/topo_antarctica/InSAR_GL_Antarctica_v02/InSAR_GL_Antarctica_v02.shp")
sf = sf.to_crs({'init': 'epsg:3031'})
bedmachine = xr.load_dataset(
    "/home/urrutyb/Documents/PhD_TiPACCS/topo_antarctica/BedMachineAntarctica_2019-11-05_v01.nc")
figure='/home/urrutyb/Documents/PhD_TiPACCS/topo_antarctica/'

# selection des données
index = np.arange(0, len(bedmachine.x), 2)
bedmachinebed = np.flip(bedmachine.bed[index, index], 0)
x = np.array(bedmachinebed.x)
y = np.array(bedmachinebed.y)

del bedmachine,bedmachinebed

mask_out_error=np.loadtxt('../../../backup.txt',delimiter=',')
# mask_out_error[index]=np.nan
# mask_out_error[index2]=np.nan
# np.savetxt('backup.txt',mask_out_error,delimiter=',')

fig=plt.figure(figsize=[12,10])

ax=plt.gca()
sf.plot(ax=ax, color='k')
im=plt.pcolormesh(x,y,mask_out_error)
cbar=plt.colorbar(im)
cbar.set_label('1 : overlap   -  0 : outside')
plt.axis('off')

# plt.title('link between the two beds +/- uncertainty')
plt.tight_layout()
plt.savefig(figure + 'uncertainty.png',dpi=300)


