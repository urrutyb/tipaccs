#!/usr/bin/python

# ce code ouvre l'ensemble des fichiers et permet de comparer les bathymetrie entre elle.
# utiliser "range" sur python 3 dans les conditions des boucles

# réalisé par Benoît URRUTY (benoit.urruty@univ-grenoble-alpes.fr ou
# b.urruty77@gmail.com)

# header
import numpy
from optparse import OptionParser
import os
import matplotlib.pyplot as plt
import matplotlib.colors as col
from matplotlib.pylab import *
import matplotlib.patches as Rectangle
import numpy as np
from matplotlib import cm
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import time
import glob
from netCDF4 import Dataset

filename = glob.glob('all_run_amundsen.nc')

# importation des autres variables
for i in range(0, len(filename), 1):
    ncfile = Dataset(filename[i], 'a')
    print(os.path.basename(filename[i]))
    globals()['alpha_%d' % i] = ncfile.variables['alpha'][:, :]
    globals()['bedrock_%d' % i] = ncfile.variables['bedrock'][:, :]
    globals()['smb_%d' % i] = ncfile.variables['smb'][:, :]
    globals()['pdc_area_%d' % i] = ncfile.variables['pdc_area'][:, :]
    globals()['pdc_melt_%d' % i] = ncfile.variables['pdc_melt'][:, :]
    globals()['zs_%d' % i] = ncfile.variables['zs'][:, :]
    globals()['zb_%d' % i] = ncfile.variables['zb'][:, :]
    globals()['mu_%d' % i] = ncfile.variables['mu'][:, :]
    globals()['h_%d' % i] = ncfile.variables['h'][:, :]
    globals()['groundedmask_%d' % i] = ncfile.variables['groundedmask'][:, :]
    globals()['ssavelocity_%d' % i] = ncfile.variables['ssavelocity'][:, :]
    globals()['h residual_%d' % i] = ncfile.variables['h residual'][:, :]
    globals()['dhdt_%d' % i] = ncfile.variables['dhdt'][:, :]


plt.figure(1, dpi=300)
plt.imshow(bedrock_0[0, :, :])
ax = plt.gca()
ax.invert_yaxis()
plt.show()

plt.figure(2, dpi=300)
plt.rcParams.update({'font.size': 10})
plt.plot(bedrock_0[0, 400, :], label='bedrock')
plt.plot(zb_0[0, 400, :], label='ice base')
plt.plot(zs_0[0, 400, :], label='ice surface')
ax = plt.gca()
plt.legend()
plt.show()


x = np.arange(0, len(bedrock_0[0, 0, :]))
y = np.arange(0, len(bedrock_0[0, :, 0]))
X, Y = np.meshgrid(x, y)

fig = plt.figure(3, figsize=(15, 15), dpi=300)
plt.rcParams.update({'font.size': 10})
ax = fig.gca(projection='3d')
#surf=ax.plot_surface(X,Y,bedrock_0[0,:,:],vmin=np.nanmin(bedrock_0), vmax=np.nanmax(bedrock_0),cmap=cm.jet)
surf = ax.plot_surface(X, Y, zb_0[0, :, :], vmin=np.nanmin(
    zb_0), vmax=np.nanmax(zb_0), cmap=cm.jet)
#surf=ax.plot_surface(X,Y,zs_0[0,:,:],vmin=np.nanmin(zs_0), vmax=np.nanmax(zs_0),cmap=cm.jet)
plt.colorbar(surf)
ax.invert_yaxis()
plt.tight_layout()
plt.show()
