# URRUTY, Benoit, IGE, Université de Grenoble
# Janvier,2020
import glob
import numpy as np
import pyvista as pv
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import matplotlib
matplotlib.rcParams['text.usetex'] = True

"""This is a way to check output simulations from ElmerIce

It need .dat files as output from the model

"""


def make_patch_spines_invisible(ax):
    """ function define some graphics features of the figure

    """
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)


url = './'
#url = '/home/urrutyb/Documents/simu/simu_bedmap/'


def pretreatment(url):
    """
    Parameters
    ----------
    url : str
          Url of the data


    Returns
    -------


    """
    # load data
    files = np.sort(glob.glob(url + 'Scalars_run*_.dat'))
    files_1 = np.sort(glob.glob(url + 'INITMIP_Scalar_OUTPUT_run*_.dat'))
    # variable INITMIP_scalar
    Time = []
    Volume = []
    Volume_Floatation = []
    Volume_change = []
    SMB = []
    BMB = []
    residual_flux = []
    Ice_Discharge = []
    Iceflux_GL = []
    GIA = []
    FIA = []
    Free = []
    # variable scalar
    timebis = []
    volumebis = []
    volumegrounded = []
    h = []
    dhdt = []
    smbbis = []
    pdc_melt = []
    h_residual = []
    convective_flux = []
    min_conflux = []
    max_conflux = []
    max_dhdt = []
    min_dhdt = []
    for i in range(0, len(files)):
        var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12 = np.loadtxt(
            files_1[i], unpack=True)
        var13, var14, var15, var16, var17, var18, var19, var20, var21, var22, var23, var24, var25 = np.loadtxt(
            files[i], unpack=True)
        Time = np.concatenate((Time, var1))
        Volume = np.concatenate((Volume, var2))
        Volume_Floatation = np.concatenate((Volume_Floatation, var3))
        Volume_change = np.concatenate((Volume_change, var4))
        SMB = np.concatenate((SMB, var5))
        BMB = np.concatenate((BMB, var6))
        residual_flux = np.concatenate((residual_flux, var7))
        Ice_Discharge = np.concatenate((Ice_Discharge, var8))
        Iceflux_GL = np.concatenate((Iceflux_GL, var9))
        GIA = np.concatenate((GIA, var10))
        FIA = np.concatenate((FIA, var11))
        Free = np.concatenate((Free, var12))
        #
        timebis = np.concatenate((timebis, var13))
        volumebis = np.concatenate((volumebis, var14))
        volumegrounded = np.concatenate((volumegrounded, var15))
        h = np.concatenate((h, var16))
        dhdt = np.concatenate((dhdt, var17))
        h_residual = np.concatenate((h_residual, var20))
        convective_flux = np.concatenate((convective_flux, var21))
        min_conflux = np.concatenate((min_conflux, var22))
        max_conflux = np.concatenate((max_conflux, var23))
        max_dhdt = np.concatenate((max_dhdt, var24))
        min_dhdt = np.concatenate((min_dhdt, var25))
    # Volume
    fig, ax = plt.subplots(figsize=[20, 10])
    ax.plot(Time, (Volume - Volume[0]) / Volume[0] * 100, label='Ice Volume')
    ax.plot(
        Time,
        (Volume_Floatation -
         Volume_Floatation[0]) /
        Volume_Floatation[0] *
        100,
        'r',
        label='Ice Volume above floatation')
    ax.set_xlabel('Time (yrs)')
    ax.set_ylabel(r'Volume variation (\%)')
    ax.set_ylabel(r'Volume variation (\%)')
    ax.legend()
    plt.savefig(url + 'figure/Volume.png')
    # Surface
    total = GIA + FIA + Free
    grounded = (GIA) / total[0] * 100
    floating = (FIA) / total[0] * 100
    free = (Free) / total[0] * 100
    fig, ax = plt.subplots(3, figsize=[20, 20])
    ax[0].plot(Time, grounded, label='Grounded Ice Area')
    ax[1].plot(Time, floating, 'r', label='Floating Ice Area')
    ax[2].plot(Time, free, 'g', label='area free of ice')
    ax[2].set_xlabel('Time (yrs)')
    ax[1].set_ylabel(r'$ \frac{Surface}{Surface Total}$ (\%)', FontSize=16)
    ax[0].legend()
    ax[1].legend()
    ax[2].legend()
    plt.savefig(url + 'figure/Surface.png')
    # dhdt
    plt.figure(3, figsize=[20, 10])
    ax = plt.gca()
    ax.plot(timebis, max_dhdt, label='max dh/dt')
    ax.plot(timebis, abs(min_dhdt), label='min dh/dt')
    ax.set_yscale('log')
    ax.set_xlabel('Time (yrs)')
    ax.set_ylabel('dhdt (m/yrs)')
    ax.legend()
    plt.savefig(url + 'figure/dhdt.png')
    # fonte
    basal = (BMB - BMB[0]) / BMB[0] * 100
    surface = (SMB - SMB[0]) / SMB[0] * 100
    fig, ax = plt.subplots(figsize=[20, 10])
    ax.plot(Time, surface, 'r', label='SMB')
    ax.plot(Time, basal, 'b', label='BMB')
    ax.set_xlabel('Time (yrs)')
    # ax.set_ylim([-0.1,0.1])
    ax.set_ylabel(r'$ \frac{Volume - Volume(t=0)}{Volume(t=0)}$ (\%)')
    ax.legend()
    plt.savefig(url + 'figure/Fonte.png')
    # Bilan de masse
    Bilan = SMB + BMB + residual_flux - Ice_Discharge - Volume_change
    plt.figure(figsize=[20, 10])
    ax = plt.gca()
    ax.plot(Time, Bilan, label='Bilan')
    ax.set_xlabel('Time (yrs)')
    ax.set_ylabel(r'Volume ($m^3$)')
    # ax.set_yscale('log')
    plt.legend()
    plt.savefig(url + 'figure/Bilan.png')
    plt.show()
    plt.close()


pretreatment(url)
