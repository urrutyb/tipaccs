# this code have been translate from the matlab code of julien brondex used in Brondex, 2019
# it take the data from the inversion to compute the parameter of
# different friction

import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
import pyvista as pv

# choose law used
# ex: law=[0,1] for 0 weertman non linear and 1 for schoof , 2 for budd
law = [0, 1, 2]

# fixed variable
m = 1 / 3  # Exposant loi Weertman NL ou Schoof
Cmax = 0.4  # Cmax loi Schoof
LineWidth = 1.3
g = 9.81
rho_ice = 910
rho_sea = 1028

# load data
files = glob.glob('/home/urrutyb/Documents/PhD_TiPACCS/real_vtu/run1_*.pvtu')
files = sorted(files)
blocks = pv.MultiBlock([pv.read(f) for f in files])
for i, block in enumerate(blocks):
    block["node_value"] = np.full(block.n_points, i)


combined = blocks.combine()
print(blocks)

# define variable from file
alpha = blocks[1].point_arrays['alpha']
z_b = blocks[1].point_arrays['zb']
bedrock = blocks[1].point_arrays['bedrock']
h = blocks[1].point_arrays['h']
U = blocks[1].point_arrays['ssavelocity']
X = blocks[1].points

x = X[:, 0]
y = X[:, 1]
u = U[:, 0]
v = U[:, 1]
GM = blocks[1].point_arrays['groundedmask']
beta = 10**alpha

# calcul de la pression effective
Neff = np.zeros(len(z_b))
for i in range(len(z_b)):
    if z_b[i] < 0:
        Neff[i] = (g * h[i] * rho_ice) + (z_b[i] * rho_sea * g)
    else:
        Neff[i] = g * h[i] * rho_ice

Neff[GM == -1] = 0

# Weertman linear
# calul du tau_b initiale
taub = beta * np.sqrt(u**2 + v**2)
taub[GM == -1] = 0

if 0 in law:
    # POUR LA LOI DE WEERTMAN NON LINEAIRE
    logical = (Neff == 0)

    # Inversion du Beta_wnl

    Beta_wnl = taub / (np.sqrt(u**2 + v**2))**m
    Beta_wnl[logical] = 10**-6
    Beta_wnl = {'X': x, 'Y': y, 'Beta_wnl': Beta_wnl}
    Beta_wnl = pd.DataFrame(Beta_wnl, columns=['X', 'Y', 'Beta_wnl'])
    Beta_wnl.to_csv('Beta_wnl.dat', sep='\t', encoding='ascii')


if 1 in law:

    # POUR LA LOI DE SCHOOF

    # Etape Final : On choisit un Clim_final et on sort Cs pour tous les noeuds qui respectent tb/N < Clim_final * Cmax avec Cmax = 0.4 et Cmax = 0.6
    # Attention ici on veut garder les noeuds flottants et leur affecté un Cs
    # petit

    Clim_final = 0.8
    Cmax04 = 0.4
    Cmax06 = 0.6

    # Pour Cmax = 04
    # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont
    # flottants
    Nf04 = (Neff == 0)
    tbN04 = (taub / Neff > Clim_final * Cmax04)
    logical = (Nf04 == 0) & (tbN04 == 1)

    # XYTaub_CsFinal_Cmax04_SV
    x04 = x[np.logical_not(logical)]
    y04 = y[np.logical_not(logical)]
    u04 = u[np.logical_not(logical)]
    v04 = v[np.logical_not(logical)]
    GM04 = GM[np.logical_not(logical)]
    Neff04 = Neff[np.logical_not(logical)]
    taub04 = taub[np.logical_not(logical)]
    Nf04 = Nf[np.logical_not(logical)]

    # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    ubFinal_Cmax04_tmp_SV = np.sqrt(u04**2 + v04**2)
    CsFinal_Cmax04_SV = taub04 / \
        (ubFinal_Cmax04_tmp_SV**m * (1 - (taub04 / (Cmax04 * Neff04))**(1 / m))**m)

    # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique
    # zones posées à écoulement rapide)
    CsFinal_Cmax04_SV[Nf04] = 10**-3

    # 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV'
    # par 'SansVisco' ou autre directement dans dossier de sauvegarde
    CsFinal_Cmax04_SV = {'X': x04, 'Y': y04,
                         'CsFinal_Cmax04_SV': CsFinal_Cmax04_SV}
    CsFinal_Cmax04_SV = pd.DataFrame(
        CsFinal_Cmax04_SV, columns=[
            'X', 'Y', 'CsFinal_Cmax04_SV'])
    CsFinal_Cmax04_SV.to_csv(
        'Cs_Cmax04_Clim08_SV.dat',
        sep='\t',
        encoding='ascii')

    # # Pour Cmax = 06

    # # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
    tbN06 = (taub / Neff > Clim_final * Cmax06)
    logical = (Nf == 0) & (tbN06 == 1)

    # XYTaub_CsFinal_Cmax06_SV
    x06 = x[np.logical_not(logical)]
    y06 = y[np.logical_not(logical)]
    u06 = u[np.logical_not(logical)]
    v06 = v[np.logical_not(logical)]
    GM06 = GM[np.logical_not(logical)]
    Neff06 = Neff[np.logical_not(logical)]
    taub06 = taub[np.logical_not(logical)]
    Nf06 = Nf[np.logical_not(logical)]

    # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    ubFinal_Cmax06_tmp_SV = np.sqrt(u06**2 + v06**2)
    CsFinal_Cmax06_SV = taub06 / \
        (ubFinal_Cmax06_tmp_SV**m * (1 - (taub06 / (Cmax06 * Neff06))**(1 / m))**m)

    # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique
    # zones posées à écoulement rapide)
    CsFinal_Cmax06_SV[Nf06] = 10**-3

    # # 6- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
    CsFinal_Cmax06_SV = {'X': x06, 'Y': y06,
                         'CsFinal_Cmax06_SV': CsFinal_Cmax06_SV}
    CsFinal_Cmax06_SV = pd.DataFrame(
        CsFinal_Cmax06_SV, columns=[
            'X', 'Y', 'CsFinal_Cmax06_SV'])
    CsFinal_Cmax06_SV.to_csv(
        'Cs_Cmax06_Clim08_SV.dat',
        sep='\t',
        encoding='ascii')
    # +end_src

# Budd law
if 2 in law:
    C_b = taub / (np.sqrt(u**2 + v**2)**m * Neff)
    logical = (Neff == 0)
    C_b[logical] = 10**-6
    C_b = {'X': x, 'Y': y, 'C_b': C_b}
    C_b = pd.DataFrame(C_b, columns=['X', 'Y', 'C_b'])
    C_b.to_csv('C_b.dat', sep='\t', encoding='ascii')
