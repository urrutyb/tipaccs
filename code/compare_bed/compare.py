import numpy as np
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from matplotlib.colors import SymLogNorm
from matplotlib import cm
import geopandas as gpd
import seaborn as sns
from matplotlib.colors import LogNorm
from scipy import ndimage
from mpl_toolkits.axes_grid1 import make_axes_locatable
import glob


def stat(x):
    mean = np.nanmean(x)
    median = np.nanquantile(x, 0.5)
    quartile1 = np.nanquantile(x, 0.25)
    quartile3 = np.nanquantile(x, 0.75)
    std = np.nanstd(x)
    return mean, median, quartile1, quartile3, std


def selectarea(values, x, y, variable):
    xx = x[int(np.where(x == values[0])[0]):int(np.where(x == values[1])[0])]
    yy = y[int(np.where(y == values[2])[0]):int(np.where(y == values[3])[0])]
    variable_set = variable[int(np.where(y == values[2])[0]):int(np.where(y == values[3])[
        0]), int(np.where(x == values[0])[0]):int(np.where(x == values[1])[0])]
    return xx, yy, variable_set


def figure(x, y, z, sf, titlevariable, colorbartitle, param):
    stati = stat(z)
    fig = plt.figure(figsize=[20, 12])
    plt.rcParams.update({'font.size': 15})
    ax = plt.gca()
    sf.plot(ax=ax, color='k')
    im = plt.pcolormesh(
        x,
        y,
        z,
        norm=SymLogNorm(
            linthresh=param[0],
            linscale=param[1],
            vmin=param[2],
            vmax=param[3]),
        cmap=cm.seismic)
    cbar = plt.colorbar(im)
    plt.xlim(x.min(), x.max())
    plt.ylim(y.min(), y.max())
    cbar.set_label(colorbartitle)
    plt.title(titlevariable +
              'Bedmap2-bedmachine \n  mean=' +
              str(stati[0]) +
              ' median=' +
              str(stati[1]) +
              ' quartile 25%=' +
              str(stati[2]) +
              '\n quartile 75%=' +
              str(stati[3]) +
              ' std=' +
              str(stati[4]))
    fig.savefig(titlevariable + '.png')
    # plt.show()
    plt.close('all')
    # fig1=plt.figure(figsize=[20,12])
    #plt.rcParams.update({'font.size': 15})
    # sns.boxplot(z)
    # plt.close('all')


def line_flow(flowline, x, y, z_bed1, z_bed2):
    # interpolate from line
    x_fl, y_fl = np.loadtxt(flowline + '.csv', skiprows=1,
                            delimiter=',', usecols=(22, 23), unpack=True)
    x_temp = (x_fl - x.min()) / 1000
    y_temp = (y_fl - y.min()) / 1000
    # profil along line interpolated
    f_bed1 = ndimage.map_coordinates(
        np.array(z_bed1), [
            y_temp, x_temp], mode='nearest')
    f_bed2 = ndimage.map_coordinates(
        np.array(z_bed2), [
            y_temp, x_temp], mode='nearest')
    nm = np.sqrt(x_temp**2 + y_temp**2)
    return x_fl, y_fl, f_bed1, f_bed2, nm


# position des crops
Ronne = [-1600000, -200000, 0, 1000000]
Amundsen = [-1900000, -1300000, -1000000, -80000]
Ross = [-800000, 800000, -1400000, -200000]

# chargement des données
sf = gpd.read_file(
    "/home/urrutyb/Téléchargements/topo_antarctica/InSAR_GL_Antarctica_v02/InSAR_GL_Antarctica_v02.shp")
sf = sf.to_crs({'init': 'epsg:3031'})
bedmachine = xr.load_dataset(
    "/home/urrutyb/Téléchargements/topo_antarctica/BedMachineAntarctica_2019-11-05_v01.nc")
bedmap_bed = np.flip(
    np.loadtxt(
        '/home/urrutyb/Téléchargements/topo_antarctica/bedmap2_ascii/bedmap2_bed.txt',
        skiprows=6),
    0)
bedmap_th = np.flip(
    np.loadtxt(
        '/home/urrutyb/Téléchargements/topo_antarctica/bedmap2_ascii/bedmap2_thickness.txt',
        skiprows=6),
    0)

# selection des données
index = np.arange(0, len(bedmachine.x), 2)
bedmachinebed = np.flip(bedmachine.bed[index, index], 0)
bedmachineth = np.flip(bedmachine.thickness[index, index], 0)
x = np.array(bedmachinebed.x)
y = np.array(bedmachinebed.y)
bedmap_bed[bedmap_bed == -9999] = np.nan
test = bedmap_bed - bedmachinebed
rap = np.abs(bedmap_bed / np.array(bedmachinebed)) - 1
thick_diff = bedmap_th - bedmachineth
thick_ra = bedmap_th / bedmachineth - 1

# figure all antarctica
figure(x, y, test, sf, 'Bedrock', 'différence of bedrock (meter)',
       [10, 0.1, test.min(), test.max()])
figure(x, y, rap, sf, 'Bedrock ratio',
       'ratio (bedmap/bedmachine-1)', [0.1, 0.1, -10, 10])
figure(x, y, thick_diff, sf, 'Thickness', 'différence of thickness (meter)', [
       10, 0.1, thick_diff.min(), thick_diff.max()])
figure(x, y, thick_ra, sf, 'Thickness ratio',
       'ratio (bedmap/bedmachine-1)', [0.1, 0.1, -10, 10])


# Select Ronne
x_Ronne, y_Ronne, z_bed = selectarea(Ronne, x, y, test)
figure(x_Ronne, y_Ronne, z_bed, sf, 'Bedrock Ronne',
       'différence of bedrock (meter)', [10, 0.1, z_bed.min(), z_bed.max()])

# Select Amundsen

x_Amundsen, y_Amundsen, z_bedmap = selectarea(Amundsen, x, y, bedmap_bed)
x_Amundsen, y_Amundsen, z_bedmachine = selectarea(
    Amundsen, x, y, bedmachinebed)
x_Amundsen, y_Amundsen, z_bed = selectarea(Amundsen, x, y, test)


flowlines = glob.glob('flow_line*.csv')
for name in flowlines:
    flowline = name[:-4]
    x_fl, y_fl, f1, f2, nm = line_flow(
        flowline, x_Amundsen, y_Amundsen, z_bedmap, z_bedmachine)
    fig = plt.figure(figsize=[20, 12])
    plt.rcParams.update({'font.size': 15})
    ax = fig.add_subplot(111)
    ax.scatter(nm, f1, s=0.5, label='bedmap')
    ax.scatter(nm, f2, s=0.5, label='bedmachine')
    ax.legend(scatterpoints=10, markerscale=2)
    ax.set_xlabel('flow line distance (km)')
    ax.set_ylabel('depth (m)')
    ax2 = fig.add_subplot(position=[0.12, 0.2, 0.3, 0.3])
    sf.plot(ax=ax2, color='k')
    im = ax2.pcolormesh(x_Amundsen, y_Amundsen, z_bedmachine, cmap=cm.seismic)
    ax2.scatter(x_fl, y_fl, s=1, c='r', label='flowline')
    ax2.set_xlim(x_Amundsen.min(), x_Amundsen.max())
    ax2.set_ylim(y_Amundsen.min(), y_Amundsen.max())
    ax2.set_xlabel('X (m)')
    ax2.set_ylabel('Y (m)')
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label('bedmachine depth of the bed (m)')
    ax2.legend(scatterpoints=10, markerscale=2)
    v1 = ax2.get_xticks()
    ax2.set_xticks(v1[::3])
    fig.savefig(flowline + '.png')
    # plt.show()


# carré
carre = [800000, 1400000, -900000, -300000]
x_carre, y_carre, z_bedmap = selectarea(carre, x, y, bedmap_bed)
x_Amundsen, y_Amundsen, z_bedmachine = selectarea(carre, x, y, bedmachinebed)
x_Amundsen, y_Amundsen, z_bed = selectarea(carre, x, y, test)
fig = plt.figure(figsize=[20, 12])
plt.rcParams.update({'font.size': 15})
ax1 = plt.subplot(221)
im = ax1.pcolormesh(
    x_carre,
    y_carre,
    z_bed,
    norm=SymLogNorm(
        linthresh=10,
        linscale=0.1,
        vmin=z_bed.min(),
        vmax=z_bed.max()),
    cmap=cm.seismic)
cbar = plt.colorbar(im, ax=ax1)
cbar.set_label('difference bedmap-bedmachine (m)')
ax1.set_xlim(x_carre.min(), x_carre.max())
ax1.set_ylim(y_carre.min(), y_carre.max())
ax1.set_title('difference')
ax2 = plt.subplot(222)
# ,norm=SymLogNorm(linthresh=10, linscale=0.1 , vmin=z_bedmap.min(), vmax=z_bedmap.max()),cmap=cm.seismic)
im = ax2.pcolormesh(x_carre, y_carre, z_bedmap, cmap=cm.seismic)
ax2.set_xlim(x_carre.min(), x_carre.max())
ax2.set_ylim(y_carre.min(), y_carre.max())
cbar = plt.colorbar(im, ax=ax2)
cbar.set_label('depth of the bed (m)')
ax2.set_title('bedmap')
ax3 = plt.subplot(223)
# ,norm=SymLogNorm(linthresh=10, linscale=0.1 , vmin=z_bedmap.min(), vmax=z_bedmap.max()),cmap=cm.seismic)
im = ax3.pcolormesh(x_carre, y_carre, z_bedmachine, cmap=cm.seismic)
ax3.set_xlim(x_carre.min(), x_carre.max())
ax3.set_ylim(y_carre.min(), y_carre.max())
ax3.set_title('bedmachine')
cbar = plt.colorbar(im, ax=ax3)
cbar.set_label('depth of the bed (m)')
fig.suptitle('le carré???')
# plt.show()
fig.savefig('carre.png')
plt.close()
