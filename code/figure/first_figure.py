import glob
import numpy as np
import pyvista as pv
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt


# load data 1
files = glob.glob('/home/urrutyb/Documents/simu/simu_bedmachine/run1_*.pvtu')
files = sorted(files)
blocks = pv.MultiBlock([pv.read(f) for f in files])
for i, block in enumerate(blocks):
    block["node_value"] = np.full(block.n_points, i)

print(blocks)

# mesh plot
for i in range(0, 6):
    pl = pv.Plotter()
    pl.add_mesh(blocks[i], scalars='ssavelocity', cmap='jet', log_scale=True)
    pl.show(cpos='xy', screenshot='ssavelocity' + str(i) + '.png')

# compare velocity
# these appear to be flow vectors of some sort.  Normalize them so we
# can get a reasonable direction comparison
flow_a = blocks[0].point_arrays['ssavelocity'].copy()
flow_b = blocks[5].point_arrays['ssavelocity'].copy()
flow_diff = flow_a - flow_b

# plot normalized vectors
pl = pv.Plotter()
pl.add_mesh(blocks[0], color='w', opacity=0.2, style='wireframe')
pl.add_arrows(
    blocks[1].points,
    flow_diff,
    mag=100,
    cmap='jet',
    label='flow_diff')
pl.add_legend()
pl.show(cpos='xy')


# contour
i = 0
contours = blocks[i].contour(
    isosurfaces=1000,
    scalars='ssavelocity',
    compute_normals=False,
    compute_gradients=False,
    compute_scalars=True,
    rng=None,
    preference='cell',
    method='contour')
pl = pv.Plotter()
pl.add_mesh(blocks[i], scalars='ssavelocity', cmap='jet', log_scale=True)
pl.add_mesh(
    contours,
    scalars='ssavelocity',
    cmap='seismic',
    log_scale=True)
pl.show(cpos='xy')
