import glob
import numpy as np
import pyvista as pv
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt


# load data 1
files = glob.glob('/home/urrutyb/Documents/PhD_Tippacs/real_vtu/run1_*.pvtu')
files = sorted(files)
blocks = pv.MultiBlock([pv.read(f) for f in files])
for i, block in enumerate(blocks):
    block["node_value"] = np.full(block.n_points, i)

print(blocks)

# load data 2
files = glob.glob('/home/urrutyb/Documents/PhD_Tippacs/real_vtu/run20_*.pvtu')
files = sorted(files)
blocks20 = pv.MultiBlock([pv.read(f) for f in files])
for i, block in enumerate(blocks):
    block["node_value"] = np.full(block.n_points, i)
print(blocks20)

# plot all data blocks
combined.plot(scalars="node_value", cpos="xy")
               screenshot = '/tmp/blocks.png')

# combined.plot(scalars="ssavelocity", cpos="xy", cmap='Blues')
combined.plot(
    scalars = "ssavelocity",
    rng = [
        0,
        700],
        cpos = "xy",
        cmap = 'Blues',
         log_scale = True)
               # screenshot='/tmp/flow.png')

# plot contour
contours = blocks[0].contour(isosurfaces = 1000, scalars = 'ssavelocity', compute_normals = False, compute_gradients = False, compute_scalars = True, rng = None, preference='cell' , method='contour')
pl=pv.Plotter()
pl.add_mesh(blocks[1], scalars = 'ssavelocity', cmap = 'jet', log_scale = True)
pl.add_mesh(
    contours,
    scalars = 'ssavelocity',
    cmap = 'seismic',
     log_scale = True)
pl.show(cpos = 'xy')

# # plot vectors with mesh in background
pl=pv.Plotter()
pl.add_mesh(blocks[1], color = 'w', opacity = 0.2, style = 'wireframe')
pl.add_arrows(
    blocks[1].points,
    blocks[1].point_arrays['ssavelocity'],
    mag = 20,
     cmap = 'Blues')
pl.show(cpos = 'xy')


# plot vectors without mesh
pl=pv.Plotter()
pl.add_arrows(
    blocks[5].points,
    blocks[5].point_arrays['ssavelocity'],
    mag = 100,
     cmap = 'jet')
pl.show(cpos = 'xy')


# compare directions
# these appear to be flow vectors of some sort.  Normalize them so we
# can get a reasonable direction comparison
flow_a=blocks[0].point_arrays['ssavelocity'].copy()
flow_b=blocks20[5].point_arrays['ssavelocity'].copy()
flow_diff
# print(flow_a,np.linalg.norm(flow_a, axis=1).reshape(-1, 1))
# # # plot normalized vectors
pl=pv.Plotter()
# #pl.add_mesh(blocks[0], color='w', opacity=0.2, style='wireframe')
pl.add_arrows(blocks[1].points, flow_a, mag = 100,cmap = 'jet', label ='flow_a')
# # pl.add_arrows(blocks[10].points, flow_b, mag=10000, color='r', label='flow_b')
pl.add_legend()
pl.show(cpos = 'xy')

# # flow_a that agrees with the mean flow path of flow_b
# agree = flow_a.dot(flow_b.mean(0))
# pl = pv.Plotter()
# pl.add_mesh(blocks[1], scalars=agree, cmap='bwr', stitle='Flow agreement with block 10')
# pl.add_mesh(blocks[10], color='w')
# pl.show(cpos='xy')

# # compare opposite direction
# agree = flow_b.dot(flow_a.mean(0))
# pl = pv.Plotter()
# pl.add_mesh(blocks[1], color='w')
# pl.add_mesh(blocks[10], scalars=agree, cmap='bwr', stitle='Flow agreement with block 10')
# pl.show(cpos='xy')


# compare ['ssavelocity-normed', 'alpha', 'bedrock', 'dhdt',
# 'groundedmask', 'h', 'h residual', 'lonlat', 'mu', 'pdc_area',
# 'pdc_melt', 'smb', 'ssavelocity', 'zb', 'zs', 'GeometryIds']

variable='ssavelocity'
# these appear to be flow vectors of some sort.  Normalize them so we
# can get a reasonable direction comparison
compare1=blocks[0].point_arrays[variable].copy()
compare2=blocks20[5].point_arrays[variable].copy()
diff=compare2 - compare1
mini=np.quantile(diff, 0.05)
maxi=np.quantile(diff, 0.95)

# print(flow_a,np.linalg.norm(flow_a, axis=1).reshape(-1, 1))
# # # plot normalized vectors
pl=pv.Plotter()
pl.add_mesh(blocks[0], color = 'w', opacity = 0.2, style ='wireframe')
pl.add_mesh(blocks[1].points,scalars = diff,cmap = 'tab20b',clim =[mini,maxi] )
pl.add_scalar_bar(title = variable, label_font_size = 12)
pl.show()
