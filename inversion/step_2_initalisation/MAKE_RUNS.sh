#!/bin/bash

module switch intel/17.2 intel/19.4
cp -r ../step_1_make_mesh/mesh_$c . 
make clean
make all
c=24
t=24
if [ $c -lt $t ]
then
    n=1
    nt=$c
    let "nc=24/$c"
else
    let "n=$c / 24"
    nt=24
    nc=1
    
fi



unset jobid0
sed "s/<MESH>/$c/g" TOPOGRAPHY.sif > TOPOGRAPHY_$c.sif
sed "s/<MESH>/$c/g" FORCING.sif > FORCING_$c.sif
sed "s/<MESH>/$c/g" INVERT_INIT.sif > INVERT_INIT_$c.sif
sed "s/<MESH>/$c/g;s/<NODES>/$n/g;s/<TASKS>/$c/g;s/<NTASKS>/$nt/g;s/<NCPUS>/$nc/g" RUN_TOPOGRAPHY.slurm > RUN_TOPOGRAPHY_$c.slurm
sed "s/<MESH>/$c/g;s/<NODES>/$n/g;s/<TASKS>/$c/g;s/<NTASKS>/$nt/g;s/<NCPUS>/$nc/g" RUN_FORCING.slurm > RUN_FORCING_$c.slurm
sed "s/<MESH>/$c/g;s/<NODES>/$n/g;s/<TASKS>/$c/g;s/<NTASKS>/$nt/g;s/<NCPUS>/$nc/g" script.slurm > script_$c.slurm



jobid=$(sbatch --parsable RUN_TOPOGRAPHY_$c.slurm)
echo "RUN TOPOGRAPHY " $c "id" $jobid
jobid0=$jobid
jobid=$(sbatch --parsable --dependency=afterany:$jobid0  RUN_FORCING_$c.slurm)
echo "RUN FORCING" $c "id" $jobid "dependency" $jobid0
jobid0=$jobid
jobid=$(sbatch --parsable --dependency=afterany:$jobid0  script_$c.slurm)
echo "INVERT INI" $c  "id" $jobid "dependency" $jobid0
