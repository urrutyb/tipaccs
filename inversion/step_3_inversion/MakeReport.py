#
# Make a report from output files of the inverse method
# make basic plots of cost function and norm of the gradient
#  and report last lines of M1QN3_<RUN_NAME>.out
# Require files: M1QN3_<RUN_NAME>.out
#                Cost_<RUN_NAME>.dat
#                CostReg_<RUN_NAME>.dat
#                GradientNormAdjoint_<RUN_NAME>.dat
from collections import deque
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from pylab import genfromtxt
import numpy as np
import sys
import glob


try:
    sys.argv[1]
except:
    print('error: try ')
    print('	python MakeReport.py RUN_NAME')
    exit()


fname = '*_%s.dat' % (sys.argv[1])
files = np.sort(glob.glob("./" + fname))

iteration = []
rms = []
cost1 = []
Jdiv = []
jregeta = []
jregbeta = []
adjoint = []
jregbeta = np.loadtxt(files[0])[:, 1]
jregeta = np.loadtxt(files[1])[:, 1]
iteration, cost1, rms = np.loadtxt(files[2], unpack=True)
Jdiv = np.loadtxt(files[3])[:, 1]
adjoint = np.loadtxt(files[4])[:, 1]

# Output from M1QN3
fname = 'M1QN3_%s.out' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()

with open(fname) as fin:
    last = deque(fin, 7)
output = ''.join(list(last))

# Cost file
fname = 'Cost_%s.dat' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()
cost = genfromtxt(fname, skip_header=3)

# Gradient norm
fname = 'GradientNormAdjoint_%s.dat' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()
grad = genfromtxt(fname, skip_header=1)

# Regularisation div dhdt
fname = 'Cost_dHdt_%s.dat' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()
regdhdt = genfromtxt(fname, skip_header=3)
with open(fname, 'r') as f:
    first_line = f.readline().strip()
print(first_line.split((',')))

# get regularisation patrameter
line = open(fname, "r").readlines()[1]
lregdhdt = float(line.split((","))[1])
print(lregdhdt)

# Regularisation Beta
fname = 'CostRegBeta_%s.dat' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()
regbeta = genfromtxt(fname, skip_header=3)
with open(fname, 'r') as f:
    first_line = f.readline().strip()
print(first_line.split((',')))

# get regularisation patrameter
line = open(fname, "r").readlines()[1]
lregbeta = float(line.split((","))[1])
print(lregbeta)

# Regularisation Eta
fname = 'CostRegEta_%s.dat' % (sys.argv[1])
try:
    with open(fname) as file:
        pass
except IOError as e:
    print('Unable to open file %s' % (fname))
    exit()
regeta = genfromtxt(fname, skip_header=0)
with open(fname, 'r') as f:
    first_line = f.readline().strip()
print(first_line.split((',')))

# get regularisation patrameter
line = open(fname, "r").readlines()[1]
lregeta = float(line.split((','))[1])
print(lregeta)

# Make the report
with PdfPages('Report_%s.pdf' % (sys.argv[1])) as pdf:
    fig = plt.figure()
    fig.suptitle("Convergence plots", fontsize=16)
    plt.subplot(3, 1, 1)
    plt.semilogx(cost[:, 0], cost[:, 2])
    plt.ylabel('rms (m/a)')
    plt.subplot(3, 1, 2)
    plt.loglog(cost[:, 0], cost[:, 1], label='$J_0$')
    plt.loglog(regbeta[:, 0], lregbeta*regbeta[:, 1],
               label=('%1.2e$ * J_{regbeta}$' % (lregbeta)))
    plt.loglog(regeta[:, 0], lregeta*regeta[:, 1],
               label=('%1.2e$ * J_{regeta}$' % (lregeta)))
    plt.loglog(regdhdt[:, 0], lregdhdt*regdhdt[:, 1],
               label=('%1.2e$ * J_{div}$' % (lregdhdt)))
    plt.legend(fontsize='10')
    plt.ylim((0.01*np.amin(cost[:, 1]), 5*np.amax(cost[:, 1])))
    plt.ylabel('$J$')
    plt.subplot(3, 1, 3)
    plt.loglog(grad[:, 0], grad[:, 1]/grad[0, 1])
    plt.xlabel('iter. number')
    plt.ylabel(r'$|\!|g|\!|/|\!|g_0|\!|$')
    plt.tight_layout()
    fig.subplots_adjust(top=0.88)
    pdf.savefig()
    plt.close()
    fig = plt.figure(figsize=(20, 20))
    ax1 = plt.subplot(3, 2, 1)
    ax1.semilogy(iteration, rms)
    ax1.set_title('rms')
    ax2 = plt.subplot(3, 2, 2)
    ax2.semilogy(iteration, cost1)
    ax2.set_title('cost')
    ax3 = plt.subplot(3, 2, 3)
    ax3.semilogy(iteration, Jdiv)
    ax3.set_title('divergence of the flux')
    ax4 = plt.subplot(3, 2, 4)
    ax4.semilogy(iteration, jregbeta)
    ax4.set_title(r'$j_{reg}$ beta')
    ax5 = plt.subplot(3, 2, 5)
    ax5.semilogy(iteration, jregeta)
    ax5.set_title(r'$j_{reg}$ eta')
    ax6 = plt.subplot(3, 2, 6)
    ax6.semilogy(iteration, adjoint)
    ax6.set_title('adjoint')
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    fig = plt.figure()
    fig.suptitle("M1QN3 last 7 lines", fontsize=16)
    fig.text(.1, .5, output)
    pdf.savefig()  # saves the current figure into a pdf page
    plt.close()
