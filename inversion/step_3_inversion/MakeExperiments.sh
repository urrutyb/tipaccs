#!/bin/bash
cmin=1
cmax=1
mesh=24 
iterations=600
simus=600

cp /scratch/cnt0021/gge6066/mchekki/LibGlace/src/librairies/elmerfem/elmerfem-64255215/builddir/elmerice/Solvers/ElmerIceSolvers.so /scratch/cnt0021/gge6066/burruty/TiPACCS/src/ElmerIceSolvers1.so

for (( nb=$cmin; nb<=$cmax; nb++ ))
do 
   DIRECTORY=OPTIM_MUMPS_pourlundi_R${nb}_$mesh
   if [ -d "$DIRECTORY" ]; then
      echo "DIRECTORY ALREADY EXISTING: " $DIRECTORY
      echo "ABORT"
      
   fi

# Create run dir
   mkdir $DIRECTORY
   cd $DIRECTORY
# copy required executables
   cp ../../../src/ElmerIceSolvers1.so .	
   cp ../Makefile  .

   cp ../README .
  module switch intel/17.2 intel/19.4
 
  make all
# copy mesh

  mkdir mesh_"$mesh" 
  cp -r ../../step_2_initalisation/mesh_$mesh/partitioning."$mesh" mesh_"$mesh"/.
  cp -r ../../step_2_initalisation/mesh_$mesh/INVERT_INIT.result.* mesh_"$mesh"/.

# 
   cp ../SIF/LINEAR_SOLVER.DIRECT LINEAR_SOLVER.txt
# Create SIF and launch 
   echo "lambdas=" $(awk -v n=$nb 'NR == n' ../LREG_avecDHDt_avecVisco.IN) "mesh=" $mesh
   sh ../SCRIPTS/RUN.sh $nb $(awk -v n=$nb 'NR == n' ../LREG_avecDHDt_avecVisco.IN ) $mesh $iterations $simus

  cd ..
done
