#!/bin/bash
#
#
if [ "$#" -ne 5 ]; then
    echo "Illegal number of parameters"
    exit
fi

# SIF FILE
sed  "s/<RUN>/$1/g;s/<Lambda1>/$2/g;s/<Lambda2>/$3/g;s/<Lambda3>/$4/g;s/<MESH>/$5/g" ../SIF/OPTIM_solver.sif  > OPTIM_R$1.sif
echo OPTIM_R$1.sif > ELMERSOLVER_STARTINFO


# SLURM FILE
sed  "s/<NAME>/OPTIM_AvecDHDT_SansVisco_R$1/g;s/<NODES>/$(($5/24))/g;s/<TASKS>/$5/g" ../SLURM/script.slurm  > script.slurm



sed  "s/<NAME>/RUN_R$1/g" ../SLURM/RUN.slurm  > RUN.slurm

#### run
sbatch --parsable script.slurm


