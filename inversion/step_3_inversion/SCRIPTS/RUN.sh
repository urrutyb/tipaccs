#!/bin/bash
#
#
if [ "$#" -ne 7 ]; then
    echo "Illegal number of parameters"
    exit
fi

c=$5
t=24
if [ $c -lt $t ]
then
    n=1
    nt=$c
    let "nc=24/$c"
else
    let "n=$c / 24"
    nt=24
    nc=1
    
fi

# SIF FILE
sed  "s/<RUN>/$1/g;s/<Lambda1>/$2/g;s/<Lambda2>/$3/g;s/<Lambda3>/$4/g;s/<MESH>/$5/g;s/<ITER>/$6/g;s/<SIMU>/$7/g" ../SIF/OPTIM.sif  > OPTIM_R$1.sif
echo OPTIM_R$1.sif > ELMERSOLVER_STARTINFO


# SLURM FILE
sed  "s/<NAME>/OPTIM_AvecDHDT_SansVisco_R$1/g;s/<NODES>/$n/g;s/<TASKS>/$5/g;s/<NTASKS>/$nt/g;s/<NCPUS>/$nc/g" ../SLURM/script.slurm  > script.slurm
sed  "s/<nb>/$1/g" ../SLURM/python.slurm > python.slurm



#### run
jobid=$(sbatch --parsable script.slurm)
echo "OPTIM id" $jobid "dependency" $jobid0
jobid0=$jobid
jobid=$(sbatch --parsable --dependency=afterany:$jobid0 python.slurm)
echo "python id" $jobid "dependency" $jobid0
jobid0=$jobid
# Transient


imin=1
imax=8


for ((i=$imin ; i<=$imax ; i++))
do
    echo $i
    echo OPTIM_R$1
    echo RUN_R$(($i-1))
    if (($i == 1))
    then 
	sed  "s/<RUN>/$i/g;s/<ID-1>/OPTIM_R$1/g;s/<MESH>/$5/g" ../SIF/RUN_TRANSIENT.sif  > RUN_R${1}_$i.sif 

    else
	sed  "s/<RUN>/$i/g;s/<ID-1>/RUN_R$(($i-1))/g;s/<MESH>/$5/g" ../SIF/RUN_TRANSIENT.sif  > RUN_R${1}_$i.sif 

    fi
    sed  "s/<R>/$1/g;s/<NUMBER>/$i/g;s/<NAME>/RUN_R${1}_$i/g" ../SLURM/RUN.slurm  > RUN_$i.slurm

  if [ ! -z "$jobid0" ];then
    jobid=$(sbatch --parsable --dependency=afterany:$jobid0  RUN_$i.slurm)
    echo "RUN" $i "id" $jobid "dependency" $jobid0
    jobid0=$jobid
  else
    jobid=$(sbatch --parsable RUN_$i.slurm)
    echo "RUN" $i "id" $jobid
    jobid0=$jobid
  fi
  
done

