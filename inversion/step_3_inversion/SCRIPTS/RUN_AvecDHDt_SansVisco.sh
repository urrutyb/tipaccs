#!/bin/bash
#
#
if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    exit
fi

# SIF FILE
sed  "s/<RUN>/$1/g;s/<Lambda3>/$2/g;s/<MESH>/$3/g" ../SIF/OPTIM.sif  > OPTIM_AvecDHDT_SansVisco_R$1.sif
echo OPTIM_AvecDHDT_SansVisco_R$1.sif > ELMERSOLVER_STARTINFO

# SLURM FILE
sed  "s/<NAME>/OPTIM_AvecDHDT_SansVisco_R$1/g;s/<NODES>/$(($3/24))/g;s/<TASKS>/$3/g" ../SLURM/script.slurm  > script.slurm
#### run
sbatch script.slurm
