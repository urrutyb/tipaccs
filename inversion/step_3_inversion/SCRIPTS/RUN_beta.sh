#!/bin/bash
#
#
if [ "$#" -ne 5 ]; then
    echo "Illegal number of parameters"
    exit
fi

# SIF FILE
sed  "s/<RUN>/$1/g;s/<Lambda1>/$2/g;s/<Lambda2>/$3/g;s/<MESH>/$5/g" ../SIF/OPTIM_beta.sif  > OPTIM_beta_R$1.sif
echo OPTIM_beta_R$1.sif > ELMERSOLVER_STARTINFO

sed  "s/<RUN>/$1/g;s/<MESH>/$5/g" ../SIF/RUN_TRANSIENT.sif  > RUN_R$1.sif

# SLURM FILE
sed  "s/<NAME>/OPTIM_AvecDHDT_SansVisco_R$1/g;s/<NODES>/$(($5/24))/g;s/<TASKS>/$5/g" ../SLURM/script.slurm  > script.slurm
sed  "s/<nb>/$1/g" ../SLURM/python.slurm > python.slurm


sed  "s/<NAME>/RUN_R$1/g" ../SLURM/RUN.slurm  > RUN.slurm

#### run
jobid=$(sbatch --parsable script.slurm)
echo "OPTIM id" $jobid "dependency" $jobid0
jobid0=$jobid
jobid=$(sbatch --parsable --dependency=afterany:$jobid0 python.slurm)
echo "python id" $jobid "dependency" $jobid0
#jobid0=$jobid
# Transient
#sbatch --parsable --dependency=afterany:$jobid0 RUN.slurm

