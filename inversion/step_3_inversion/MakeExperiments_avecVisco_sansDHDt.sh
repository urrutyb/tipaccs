#!/bin/bash
cmin=1
cmax=1
mesh=24 

for (( c=$cmin; c<=$cmax; c++ ))
do 
   DIRECTORY=OPTIM_sansDHDt_avecVisco_R$c
   if [ -d "$DIRECTORY" ]; then
      echo "DIRECTORY ALREADY EXISTING: " $DIRECTORY
      echo "ABORT"
      
   fi

# Create run dir
   mkdir $DIRECTORY
   cd $DIRECTORY
# copy required executables
   cp ../Makefile  .
   make all
# copy mesh
  mkdir mesh_"$mesh" 
  cp -r ../../step_2_initalisation/mesh_24/partitioning."$mesh" mesh_"$mesh"/.
  cp -r ../../step_2_initalisation/mesh_24/INVERT_INIT.result.* mesh_"$mesh"/.

# 
   cp ../SIF/LINEAR_SOLVER.GMRES LINEAR_SOLVER.txt
# Create SIF and launch 
   sh ../SCRIPTS/RUN_sansDHDt_avecVisco.sh $c $(awk -v n=$c 'NR == n' ../LREG_sansDHDt_avecVisco.IN ) $mesh 
#
  cd ..
done
