!! Name of the RUN 
$name="R<RUN>"
$niter=<ITER>
$nsimu=<SIMU>
!! Regularisation parameter
$Lambda1=<Lambda1>
$Lambda2=<Lambda2>
$Lambda3=<Lambda3>
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$DATA_DIR="/scratch/cnt0021/gge6066/burruty/TIPACCS/DATA"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
include /scratch/cnt0021/gge6066/burruty/TiPACCS/PARAMETERS/Physical_Params.IN
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
  Mesh DB "." "mesh_<MESH>"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Constants
! Used for Neumann condition
  Water Density = Real $ rhow
  Sea Level = Real 0.0
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Coordinate System  = Cartesian 2D
  Simulation Type = Steady State

  Steady State Min Iterations = 1
  Steady State Max Iterations = $niter

  Post File = "OPTIM_$name$.vtu"
  OutPut File = "OPTIM_$name$.result"
  Output Intervals = 50

  Restart File = "INVERT_INIT.result"
  Restart Before Initial Conditions = logical True
  Restart Time = Real 0.0

  max output level = 3
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body 1
  Initial Condition = 1
  Equation = 1
  Body Force = 1
  Material = 1
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1
  beta = Equals alpha
  eta = Real 1.0

End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
!# gravity for BodyForce 2 (1D-case) or BodyForce 3 (2D plane view case)
  Flow BodyForce 1 = Real 0.0            
  Flow BodyForce 2 = Real 0.0              
  Flow BodyForce 3 = Real $gravity

! For Cost DhDt
  Top Surface Accumulation = Equals smb
  Bottom Surface Accumulation = Variable PDC_MELT
     REAL PROCEDURE "USFs_Ronnie_inversion" "MinusA"
  Observed dhdt = Real 0.0
!
  DJDBeta_Reg_var Passive = Variable GroundedMask
     Real procedure "USFs_Ronnie_inversion" "PassiveCond_DJDBReg"

  Passive Element Min Nodes = Integer 1
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Material 1
  Viscosity Exponent = Real $1.0e00/n
  Critical Shear Rate = Real 1.0e-12

  SSA Mean Density = Real $rhoi
  SSA Mean Viscosity = Variable eta, Mu
     REAL procedure "USFs_Ronnie_inversion" "SSAViscosity"
  SSA Mean Viscosity Derivative = Variable eta, Mu
     REAL procedure "USFs_Ronnie_inversion" "SSAVIscosity_d"


!!! linear or weertman
  SSA Friction Law = String "linear"
  
!!! Friction coefficient
  SSA Friction Parameter = Variable beta
     REAL procedure "ElmerIceUSF" "TenPowerA"

  SSA Friction Parameter Derivative = Variable beta
     REAL procedure "ElmerIceUSF" "TenPowerA_d"

  !! For Save scalar to compute Grounded Area
  GroundedAreaCoeff = Variable Groundedmask
        Real procedure "USFs_Ronnie_inversion" "GroundedAreaCoeff"
  !! For Save scalar to compute mass flux (=H*SSA_UV)
   Flux = Equals H


     

End
 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 1
  Equation = "SSA"
  Variable = -dofs 2 "SSAVelocity"

  Procedure = "ElmerIceSolvers" "AdjointSSA_SSASolver"

!# Set true to set friction to 0 when ice is floating
!   Require: GroundedMask and Bedrork variables
  Sub-Element GL parameterization = logical True
!# You can set the number of IP points to evaluated the friction in the
!   first floating elemnt
  GL integration points number = Integer 20

!# Keywords related to linear system
  include LINEAR_SOLVER.txt

!# Keywords related to non-linear iterations 
!  for the accuracy of the gradient computation
!   Newton has to be used if the problem is non linear
  Nonlinear System Max Iterations = 50
  Nonlinear System Convergence Tolerance  = 1.0e-08
  Nonlinear System Newton After Iterations = 12
  Nonlinear System Newton After Tolerance = 1.0e-05
  Nonlinear System Relaxation Factor = 1.00

  Steady State Convergence Tolerance = Real 1.0e-12

  Exported Variable 1 = -global CostValue

  Exported Variable 2 = Var[beta:1 eta:1]
  Exported Variable 2 DOFs = Integer 2

  Exported Variable 3 = DJDVar[DJDBeta:1 DJDEta:1]
  Exported Variable 3 DOFs = Integer 2

  Exported Variable 4 = -nooutput "Velocityb"
  Exported Variable 4 DOFs = Integer 2
End

!!! Compute Cost function
Solver 2
  Equation = "Cost"
  !## No need to declare a variable, this is done internally to insure that Solver structures exist

    procedure = "ElmerIceSolvers" "Adjoint_CostDiscSolver"

    !# If standard errors are given they are used to normalise the cost function
     Use standard error = Logical TRUE
    !# a min value for the standard error to avoid a division by 0
    !# standard error Min Value = Real [default: 100*AEPS]

   !# Name of the variable that contain the cost value
   !#  must exist and can be a global variable
   Cost Variable Name = String "CostValue"

   !# a multiplicatif factor can be used to scale the cost function
   Lambda = Real 1.0
   
   !# Name of an ascii file that will contain the cost value as
   !## Time, Lambda*J, RMS=sqrt(2J/(Nobs*Lambda))
   Cost Filename = File "Cost_$name$.dat"

   !# The name of the model variable that is observed
   Observed Variable Name = String "SSAVelocity"

   !# Name of the file that contains the observations
   !## can be netcdf (with .nc extension) or ascii (any other extension)
   !## reading netcdf is only possible for observations in a 2D plane
   !## Coordinates dimension of the Obs. must be consistent with Pb dimension;
   !##  i.e. CoordsystemDim if solver is on the bulk or (CoordsystemDim-1) if solver is on a boundary
   Observation File Name = File "/scratch/cnt0021/gge6066/burruty/TiPACCS/DATA/VELOCITY/antarctic_ice_vel_phase_map_v01.nc"

   !# If the variable is a vector, how many component do we observe?
   !## it will be the first *n* components
   Observed Variable dimension = Integer 2
   
   !# Name of the variable that contain the observation
   !## if Observed Variable dimension = 1
   !Netcdf Var Name = File "" [default "Name Of The model Observed Variable"]
   !## else
   Netcdf Var 1 Name = File "VX"
   Netcdf Var 2 Name = File "VY"
   !# The solver will look for the attribute _FillValue to check if the data exist or not

   !# IF Use standard error=TRUE, the name of the variables for standard error
   Netcdf Std Var 1 Name = File "ERRX"
   Netcdf Std Var 2 Name = File "ERRY" 
   
end
!!! Compute Cost function DHDT
Solver 3
  Equation = "Cost_DHDT"
  !## No need to declare a variable, this is done internally to insure that Solver structures exist

   procedure = "ElmerIceSolvers" "AdjointSSA_CostFluxDivSolver"
   
   !# Should we initialise Cost and velocityb to 0 here
   !# Set to false is a cost function has been computed in a previous solver
   Reset Cost Value = Logical False

   !!! Problem Dimension = Integer 2 !2D mesh and 2D SSA Solution !!useful?
   !# Name of Cost Variable
   Cost Variable Name = String "CostValue"

   !# Multiplicative scaling parameter for the cost
   Lambda= Real $Lambda1

   !# Should we compute gradient with respect to bottom surface elevation
   !# hard coded initialization to zero in this solver
   Compute DJDZb = Logical False
   
   !# Should we compute gradient with respect to top surface elevation
   !# hard coded initialization to zero in this solver
   Compute DJDZs = Logical False

   ! save the cost as a function of iterations (iterations,Cost,rms=sqrt(2*Cost/Ndata)
   Cost Filename = File "Cost_dHdt_$name$.dat"
end
!!!!  Adjoint Solution
Solver 4
  Equation = "Adjoint"
  Variable = -nooutput Adjoint
  Variable Dofs = 2

  procedure = "ElmerIceSolvers" "Adjoint_LinearSolver"

!Name of the flow solution solver
  Direct Solver Equation Name = string "SSA"
 
  include LINEAR_SOLVER.txt
End
!!!!!  Compute Derivative of Cost function / Beta
Solver 5
  Equation = "DJDBeta"
    procedure = "ElmerIceSolvers" "AdjointSSA_GradientSolver"

    !# name of the variable solution of the direct problem 
    Flow Solution Name = String "SSAVelocity"
    
    !# name of the variable solution of the adjoint problem 
    Adjoint Solution Name = String "Adjoint"

    !# Should we compute the gradient w.r.t. **SSA Friction Parameter** [Material]
    Compute DJDBeta = Logical True   ! Derivative with respect to the SSA Mean Viscosity
    Reset DJDBeta = Logical True     ! This is defaults but yes initialisation must be done here


    !# Same for **SSA Mean Viscosity** [Material]
    !# Var Name hard coded DJDEta
    Compute DJDEta = Logical True   ! Derivative with respect to the SSA Mean Viscosity
    Reset DJDEta = Logical True     ! This is defaults but yes initialisation must be done here

    
 end
!  Compute Regularistaion term
Solver 6
  Equation = "DJDBeta_Reg"
    procedure = "ElmerIceSolvers" "Adjoint_CostRegSolver"
    !## No need to declare a variable, this is done internally to insure
    !    that Solver structures exist
    
    Problem Dimension=Integer 2

    !# Name of an ascii file that will contain the cost value as
    !## Time, J_{reg}, RMS=sqrt(2J_{reg}/Area)
    Cost Filename=File "CostRegBeta_$name$.dat"

    !# The name of the model variable V
    !## if not found will look for the keyword "CostReg Nodal Variable" 
    !    in the body force.
    Optimized Variable Name= String "beta"

    !# The name of the model variable that will contain the derivative
    !  of J w.r.t. the input variable
    Gradient Variable Name= String "DJDBeta"

    !# Name of the variable that contain the cost value
    !#  must exist and can be a global variable
    Cost Variable Name= String "CostValue"

    !# a multiplicatif factor can be used to scale the cost function
    Lambda= Real $Lambda2

    !# True if cost function and gradient must be initialised 
    !   to 0 in this solver
    Reset Cost Value= Logical False  !=> DJDapha already initialized in solver DJDBeta; switch off initialisation to 0 at the beginning of this solver

    !# Switch to regularisation from *prior*
    A priori Regularisation= Logical False
end
Solver 7
  Equation = "DJDEta_Reg"
    procedure = "ElmerIceSolvers1" "Adjoint_CostRegSolver"
    !## No need to declare a variable, this is done internally to insure
    !    that Solver structures exist
    
    Problem Dimension=Integer 2

    !# Name of an ascii file that will contain the cost value as
    !## Time, J_{reg}, RMS=sqrt(2J_{reg}/Area)
    Cost Filename=File "CostRegEta_$name$.dat"

    !# The name of the model variable V
    !## if not found will look for the keyword "CostReg Nodal Variable" 
    !    in the body force.
    Optimized Variable Name= String "eta"

    !# The name of the model variable that will contain the derivative
    !  of J w.r.t. the input variable
    Gradient Variable Name= String "DJDEta"

    !# Name of the variable that contain the cost value
    !#  must exist and can be a global variable
    Cost Variable Name= String "CostValue"

    !# a multiplicatif factor can be used to scale the cost function
    Lambda= Real $Lambda3

    !# True if cost function and gradient must be initialised 
    !   to 0 in this solver
    Reset Cost Value= Logical False  !=> DJDapha already initialized in solver DJDBeta; switch off initialisation to 0 at the beginning of this solver

    !# Switch to regularisation from *prior*
    A priori Regularisation= Logical False
end
!!!!! Optimization procedure : Parallel only
Solver 8
  Equation = "Optimize_m1qn3"
  procedure = "ElmerIceSolvers" "Optimize_m1qn3Parallel"

  !# name of the variable that contains value of *J* for the current state
  Cost Variable Name = String "CostValue"

  !# name of the *state variable* x
  Optimized Variable Name = String "Var"

  !# name of the gradient of *J* w.r.t. *x*:
  Gradient Variable Name = String "DJDVar"

  !# [Optionnal] : compute and save the euclidean norm of *g*
  gradient Norm File = File "GradientNormAdjoint_$name$.dat"


 ! M1QN3 Parameters
  M1QN3 dxmin = Real 1.0e-10
  M1QN3 epsg = Real  1.e-7
  M1QN3 niter = Integer $niter
  M1QN3 nsim = Integer $nsimu
  M1QN3 impres = Integer 5
  M1QN3 DIS Mode = Logical False
  M1QN3 df1 = Real 0.5
  M1QN3 normtype = String "dfn"
  M1QN3 OutputFile = File  "M1QN3_$name$.out"
  M1QN3 ndz = Integer 20

end

!!!!!
Solver 9
     Exec Solver = After All

     Equation = "Save Scalars"
     Procedure = File "SaveData" "SaveScalars"

     Filename = File "Scalars_OPTIM_$name$.dat"

     Parallel Reduce = logical true

! int H = Volume
     Variable 1 = "H"
     Operator 1 = "int"
! area
     Variable 2 = "H"
     Operator 2 = "volume"
! area grounded
     Variable 3 = "groundedmask"
     Operator 3 = "volume"
     Coefficient 3 = "GroundedAreaCoeff"

! OUT Flow
     Variable 4 = "SSAVelocity"
     Operator 4 = "convective flux"
     Coefficient 4 = "Flux"

End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Equation 1
  Active Solvers(8) = 1 2 3 4 5 6 7 8 
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Boundary Condition 1
  Name = "Ice Front"
  Target Boundaries = 1

  Calving Front = logical true

  Save Scalars = Logical True
End



