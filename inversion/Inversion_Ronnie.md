

# Inversion of the viscosity and the friction parameter for the Ronne-Fichner Ice Shelf

[TOC]



### Notations

| Notation                          |                      Description                       | Unités                  |
| --------------------------------- | :----------------------------------------------------: | ----------------------- |
| $A$                               |           Fluidité de la glace (Rate factor)           | $[Pa^{−n} a^{−1} ]$     |
| $a_b$                             |      Accrétion/ablation sur la surface inférieure      | $[m\ a^{−1} ]$          |
| $a_s$                             |    Accumulation/ablation sur la surface supérieure     | $[m\ a^{−1} ]$          |
| $b$                               |                   Altitude du socle                    | $[m]$                   |
| $C$                               |               Coefficient de frottement                | $[Pa\ m^{−m}\ a^{m} ]$  |
| $g$                               |              Accélération de la pesanteur              | $[m\ a^{-2}]$           |
| $H$                               |                   Épaisseur de glace                   | $[m]$                   |
| $H|_{cf}$                         | Épaisseur de glace au front de la plateforme flottante | $[m]$                   |
| $I_{\dot\epsilon2}$               |             Second invariant du tenseur   ̇             | $[a^{-1}]$              |
| $m$                               |     Exposant des lois de frottement relatif à u b      |                         |
| $N$                               |                   Pression effective                   | $[Pa]$                  |
| $\mathbf{n}$                      |         Vecteur normal à la surface considérée         |                         |
| $n$                               |               Exposant de la loi de Glen               |                         |
| $p$                               |                   Pression isotrope                    | $[Pa]$                  |
| $p_i$                             |  Pression cryostatique (poids de la colonne de glace)  | $[Pa]$                  |
| $p_w$                             |             Pression d’eau sous-glaciaire              | $[Pa]$                  |
| $Q$                               |                  Énergie d’activation                  | $[J\ mol^{-1}]$         |
| $R$                               |               Constante des gaz parfaits               | $[J\ mol^{-1}\ K^{-1}]$ |
| $T$                               |                Température de la glace                 | $[K]$                   |
| $t$                               |                         Temps                          | $[a]$                   |
| $\mathbf{u}=(u,v,w)$              |                    Vecteur vitesse                     | $[m\ a^{-1}]$           |
| $\mathbf{x}_{gl}=(x_{gl},y_{gl})$ |        Vecteur position de la ligne d’échouage         | $[m]$                   |
| $z$                               |                  Coordonnée verticale                  | $[m]$                   |
| $z_b$                             |           Altitude de la surface inférieure            | $[m]$                   |
| $z_s$                             |           Altitude de la surface supérieure            | $[m]$                   |
| $z_{sl}$                          |         Niveau moyen de la surface des océans          | $[m]$                   |
| $\boldsymbol{\dot\epsilon}$       |            Tenseur des taux de déformation             | $[a^{-1}]$              |
| $\eta_i$                          |                 Viscosité de la glace                  | $[Pa\ a]$               |
| $\bar\eta$                        |      Viscosité de la glace intégrée verticalement      | $[Pa\ a\ m]$            |
| $\rho_i$                          |              Masse volumique de la glace               | $[kg\ m^{-3}]$          |
| $\rho_w$                          |                Masse volumique de l’eau                | $[kg\ m^{-3}]$          |
| $\boldsymbol{\sigma}$             |           Tenseur des contraintes de Cauchy            | $[Pa]$                  |
| $\mathbf{\tau}_b$                 |       Vecteur contrainte de cisaillement basale        | $[Pa]$                  |

## La physique du glacier



![x](/home/urrutyb/Documents/text955.png)

<img src="/home/urrutyb/Documents/text859.png" alt="text859" style="zoom:25%;" />

L’équation de flottaison permet de définir les zones posées ou flottantes du glacier émissaire. Il est ainsi possible de déterminer la position de la ligne d’échouage : 
$$
\left\{
\begin{array}{ll}
z_b(x,y,t) = b(x,y)\\
z_b(x,y,t)=-H(x,y)\frac{\rho_i}{\rho_w}>b(x,y)
\end{array}
\right.
$$


Position de la ligne d’échouage peut être défini par :
$$
H(x_{gl},y_{gl})+b(x_{gl},y_{gl})\frac{\rho_w}{\rho_i}=0
$$
Les équations de la SSA (Shallow Shelf Approximation) nous permettent d’obtenir une approximation de la vitesse  $\mathbf u(u,v)$ du glacier. On considère alors que la vitesse vertical est constante :
$$
\frac{\partial u}{\partial z}\approx 0, \hspace{1cm} \frac{\partial v}{\partial z}\approx 0
$$
Les vitesses horizontales ne vont alors dépendre que des coordonnées horizontales

 Afin de les appliquer sur l’ensemble du glacier émissaires un terme de friction basal est ajouté, ceci est valable tant que le glacier est posé un socle rocheux sédimentaire/souple de rugosité faible.
$$
4\frac{\partial}{\partial x}(\bar\eta H  \frac{\partial u}{\partial x}) + 2\ \frac{\partial}{\partial x}(\bar\eta H\frac{\partial v}{\partial y}) + \frac{\partial}{\partial y}(\bar\eta H(\frac{\partial u}{\partial y} +  \frac{\partial v}{\partial x})) - \tau_{b,x} =\rho_igh\frac{\partial z_s}{\partial x}
$$

$$
4\frac{\partial}{\partial y}(\bar\eta \frac{\partial v}{\partial y}) + 2\frac{\partial}{\partial y}(\bar\eta\frac{\partial u}{\partial x}) + \frac{\partial}{\partial x}(\bar\eta (\frac{\partial v}{\partial x} + \frac{\partial u}{\partial y})) - \tau_{b,y} =\rho_igh\frac{\partial z_s}{\partial y}
$$

où:

- $\bar\eta$ est la viscosité effective moyenne intégrée verticalement 
$$
\bar\eta=\eta_0 I^{\frac{1}{n}-1}_{\dot\epsilon_2}
$$

$$
\eta_0 = \frac{1}{H}\int^{z_s}_{z_b}\frac{1}{2}A^{-1/n}dz
$$


  *A*  est la fluidité de la glace (rate factor) et $n$ une constante, souvent n=3. $I_{\dot\epsilon_2}$ est le second invariant du tenseur de contrainte $\dot\epsilon$. 
$$
I_{\dot\epsilon_2} =\sqrt{1/2\dot\epsilon_{ij}\dot\epsilon_{ji}} \\\dot\epsilon_{ij}=\frac{1}{2}(\frac{\partial \mathbf u_i}{\partial X_j} + \frac{\partial \mathbf u_j}{\partial X_i})
$$


  La fluidité A dépend de la température de la glace et est défini la loi d’Arrhenius :
$$
A = A_0 e^{-Q/(RT')}
$$
  où $A_0 \approx 1,26 \times 10^{-5} Pa^{-3}.a^{-1}$ est une valeur de référence appelé facteur pré-exponentiel, $Q=6 \times 10^{4} J.mol^{-1}$ l’énergie d’activation d’Arrhenius, $R$ la constance des gaz parfait et $T$ la température. Ces constantes sont valables pour $T’ \leq 263.15K$ avec :
$$
T'=T-T_m+T_0=T+\beta p \ avec\ \beta=7\times 10^{-8} KPa^{-1}
$$
  $p=\rho_i g h_i$ pression de glace pour une profondeur

- $\tau_b $ est la contrainte de cisaillement basale définie pour chaque composante x,y qui s’écrit $\tau_b = C \mathbf{u} $ avec $C$  le paramètre de friction. 

### Conditions aux limites

il existe deux limites à notre domaine :

- sur le front de mer “Calving Front”, la limite est défini tel que les contraintes normales s’opposent à la pression normales exercés par l’eau (condition de Neumann) :

  $\boldsymbol{\sigma}|_{cf} \cdot \mathbf{n} =-p_w\mathbf{n}$ 	avec	 $p_w = \left\{\begin{array}{ll} 0,\hspace{2cm} for \ z \geq z_{sl} \\ \rho_{w}gh,\hspace{1.1cm} for \ z \leq z_{sl}\end{array}
\right.$
  
  Dans le cadre de la SSA, cette condition s’écrit :
  $$
  4H\bar\eta \frac{\partial u}{\part x}|_{cf}n_x+2H\bar\eta\frac{\part v}{\part y}|_{cf}n_x+H\bar\eta(\frac{\part u}{\part y}+\frac{\part v}{\part x})|_{cf}n_y = \frac{g}{2}(\rho_iH|_{cf}^2-\rho_wH_{sub}|_{cf}^2)n_x\\
  4H\bar\eta \frac{\partial u}{\part x}|_{cf}n_y+2H\bar\eta\frac{\part v}{\part y}|_{cf}n_y+H\bar\eta(\frac{\part u}{\part y}+\frac{\part v}{\part x})|_{cf}n_x = \frac{g}{2}(\rho_iH|_{cf}^2-\rho_wH_{sub}|_{cf}^2)n_y
  $$
  
- sur les marges du bassin “Sides”, la limite est défini pour que la vitesses normales à la limite soit nulle. (condition de Diriclet)

  $\boldsymbol u|_{side} \cdot \textbf{n}=0$

## Initialisation

Pour réaliser l’application numérique de ces équations nous avons besoin d’interpoler des jeux de données  et de calculer certaines variables.

Les données à interpoler :

- La topographie du socle rocheux ainsi que l’épaisseur de glace provenant de BedMachine
- L’accumulation à la surface du glacier $a_s$ issue d’une simulation MAR
- La viscosité moyenne (Paterson) qui correspondra à notre première estimation sur la viscosité
- Les vitesses de surface observées $u_{obs}$ (MEaSUREs)

Nous pouvons à présent calculer les autres variables :

- L’équation de flottaison nous permet de calculer la topographie de surface $z_s$ et à la base de la glace $z_b$. Nous obtenons aussi un masque des zones posées ou flottantes. Ce masque prend pour valeur :

  - -1 dans les zones flottantes
  - 0 à la ligne d’échouage
  - 1 dans les zones posées

- La fonte sous les plateformes $a_b$ est défini par la paramétrisation issue Pollard & De Conto  

- Le gradient nodal de la surface ce qui correspond à une pente.

- La première estimation du paramètre de friction est défini par : $C=rho*g*(zs-zb)*\alpha/||u||^m$ 

  $\alpha$ est la pente $=\sqrt{\frac{\partial z_s}{\partial x}²+\frac{\partial z_s}{\partial y}²}$

  $m$ est l’exposant de friction et est égal à 1 dans le cadre d’une loi de friction linéaire

  Une valeur minimale $C_{min}=10^{-6}$ et maximale $C_{max}=1$ de ce paramètre de friction sont défini.

  Un changement de variable est effectué et notre paramètre de friction est contenu dans une variable $\beta$ tel que $\beta=\log10(C)$. Cependant, notre première estimation de ce $\beta$ est appelé $\alpha$ avant la première itération de l’inversion.

## Inversion

L’objectif de l’inversion est de déterminer le champ de viscosité et le paramètre de friction permettant de correspondre au mieux aux valeurs de vitesse de surface observées. 

Cependant, ce ne sont pas les variables C et $\mu$ qui sont directement inversées. Un changement de variable est effectué tel que :

<u>SSA Mean Viscosity</u> :
$$
\bar\mu=\eta ^2 \times \bar\mu_{ini}
$$
cette variable est utilisé dans le calcul de la SSA

<u>SSA Friction Parameter</u> :
$$
C=10^\beta
$$
Le paramètre de friction $C$ n’est calculé que pour les zones flottantes. Mais $\beta$ est initialisé sur l’ensemble du maillage et gardera une valeur fixe  $\beta=-6$ sous les plateformes.

Nous allons donc inverser les paramètres $\eta$ et $\beta$. Cela permet d’obtenir une viscosité et une paramètre de friction qui seront toujours positif.

Afin d’appliquer une pénalisation sur la divergence du flux, il nous est nécessaire de définir le bilan de masse du glacier. 

$\dot a=a_s+a_b$

Notre écart aux observation	 est alors défini tel que:


$$
(\frac{\partial H}{\partial t}-\frac{\partial H}{\partial t}^{obs})^2=(\dot{a}-div(\overline uH)-\frac{\partial H}{\partial t}^{obs})^2
$$


Afin de calculé l’optimisation, certaines constantes et variables nécessites d’être définies :

<u>Viscosity exponent</u> :
$$
Viscosity\ exponent = \frac{1}{n}\ avec\ n=3
$$

utilisé pour la calcul de la viscosité dans la SSA

 <u>Critical shear rate<u> :
$$
\tau_c = 1.0\times10^{-12}\  Pa
$$
Variable utilisé comme valeur limite car l’exposant de l’invariant est négatif 

<u>Mean density</u> :
$$
\rho_i = 917\ kg.m^{-3}
$$
Variable utilisé dans le calcul de vitesse SSA, de la flottaison.

<u>SSA Mean Viscosity Derivative</u> :
$$
\frac{\part\mu}{\part \eta}=2\times \eta \times\mu
$$
permet de calculer le gradient pour optimiser $\eta$

<u>($\eta^{-2/3}$) SSA Friction parameter derivative</u> :
$$
\frac{\partial C}{\partial \beta}=10 ^ {\beta}\times ln 10
$$
permet de calculer le gradient pour optimiser $\beta$

<u>Grounded area coefficient</u> : donne la valeur 1 si la glace est posé et 0 si nous sommes à la ligne d’échouage. il est utilisé comme coefficient pour le calcul de la surface posée

Voici les équations de la SSA dont le nom des variables est adaptées selon nos dénominations. Nous résolvons les équations :

$$
\frac{\partial}{\partial x}(2\bar\mu h (2 \frac{\partial u}{\partial x} + \frac{\partial v}{\partial y})) + \frac{\partial}{\partial y}(\bar\mu h (\frac{\partial u}{\partial y} +  \frac{\partial v}{\partial x})) - Cu =\rho_igh\frac{\partial z_s}{\partial x}
$$

$$
\frac{\partial}{\partial y}(2\bar\mu h (2 \frac{\partial v}{\partial y} + \frac{\partial u}{\partial x})) + \frac{\partial}{\partial x}(\bar\mu h (\frac{\partial v}{\partial x} + \frac{\partial u}{\partial y})) - Cv =\rho_igh\frac{\partial z_s}{\partial y}
$$



et obtenons $V(u,v)$

## Modèle inverse 

### La fonction coût

$$
J_{tot}=J_v+\lambda_{1}J_{div}+\lambda_{2}J_{\beta}+\lambda_{3}J_{\eta}
$$

Les fonctions coûts $J_{div},\ J_{\beta}\ et \ J_{\eta}$ sont assorti d’un facteur multiplicateur $\lambda$ permettant de leur donner du poids dans le calcul de la fonction coût totale.

#### La fonction coût sur les vitesses

La fonction coût $J_v$ est une comparaison des vitesses calculées par la SSA aux vitesses observés.
$$
J_v = \sum_1^{Nobs}{\frac{1}{2}(U_{SSA} - U_{Obs})^2}
$$

#### La pénalisation de la divergence du flux.

$$
J_{div}=\frac{1}{2}\int (\dot{a}-div(\overline uH))^2 \\
$$

#### Les fonctions de régularisations

Les fonctions coût de régularisations peuvent être calculé par la dérivé au premier degré des variables nodales à optimiser $\eta$ et $\beta$.  Elle n’est pas calculé sur les zones posées pour $\beta$
$$
J_{reg} = \int_{\Omega} 0.5 (|dV/dx|)^2 d\Omega
\\with\ V \ the\ nodal\ variable
$$

### Le Gradient et sa minimisation

Le gradient est calculé pour chaque fonction coût relativement au variables $\eta$ et $\beta$ par la méthode adjointe. Théoriquement, la solution de notre optimisation est une fonction coût $J_{tot}$ nulle.

Quand les gradients de $J_{tot}$ sont calculés, ils sont ensuite minimisé par une méthode de descente de gradient. Elmer/Ice utilise l’algorithme M1QN3 pour minimiser le gradient.



## Post traitement

Un peu de post-traitement est effectué dans le fichier SIF au travers du solver “SaveScalars”

<u>int</u> : permet de calculer l’intégrale d’une variable sur tout le maillage (par exemple avec H on calcule le volume total de glace)

<u>volume</u> : permet de calculer le volume du maillage dans notre cas à  dimensions il s’agit en fait d’une aire

<u>convective flux</u> : permet de calculé le flux au niveau des limites du maillage en utilisant un coefficient “Flux” qui vaut H car on est dans un modèle 2D.