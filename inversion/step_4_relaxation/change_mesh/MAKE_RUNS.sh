
imin=1
imax=4

unset jobid0

#imax=41
#jobid0=4084716

for ((i=$imin ; i<=$imax ; i++))
do
  echo :$i
  sed  "s/<ID>/$i/g;s/<ID-1>/$((i-1))/g" SIFs/RUN_CONTROL.sif > RUN_$i.sif
  sed  "s/<ID>/$i/g" SIFs/RUN.slurm > RUN_$i.slurm
  if [ ! -z "$jobid0" ];then
    jobid=$(sbatch --parsable --dependency=afterany:$jobid0  RUN_$i.slurm)
    echo "RUN" $i "id" $jobid "dependency" $jobid0
    jobid0=$jobid
  else
    jobid=$(sbatch --parsable RUN_$i.slurm)
    echo "RUN" $i "id" $jobid
    jobid0=$jobid
  fi
  
done
