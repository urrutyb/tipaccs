#!/bin/bash
#
#
if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    exit
fi

c=$5
t=24
if [ $c -lt $t ]
then
    n=1
    nt=$c
    let "nc=24/$c"
else
    let "n=$c / 24"
    nt=24
    nc=1
    
fi


# Transient


imin=1
imax=8


for ((i=$imin ; i<=$imax ; i++))
do
    echo OPTIM_R$1
    if (($i == 1))
    then 
	sed  "s/<RUN>/$i/g;s/<ID-1>/OPTIM_R$1/g;s/<MESH>/$2/g" ../SIF/RUN_TRANSIENT.sif  > RUN_R${1}_$i.sif 

    else
	sed  "s/<RUN>/$i/g;s/<ID-1>/RUN_R$(($i-1))/g;s/<MESH>/$2/g" ../SIF/RUN_TRANSIENT.sif  > RUN_R${1}_$i.sif 

    fi
    sed  "s/<R>/$1/g;s/<NUMBER>/$i/g;s/<NAME>/RUN_R${1}_$i/g" ../RUN.slurm  > RUN_$i.slurm

  if [ ! -z "$jobid0" ];then
    jobid=$(sbatch --parsable --dependency=afterany:$jobid0  RUN_$i.slurm)
    echo "RUN" $i "id" $jobid "dependency" $jobid0
    jobid0=$jobid
  else
    jobid=$(sbatch --parsable RUN_$i.slurm)
    echo "RUN" $i "id" $jobid
    jobid0=$jobid
  fi
  
done

