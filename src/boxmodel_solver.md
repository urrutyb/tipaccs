# Boxes model Solver



## General information

- **Solver Fortran File:** BoxModel.F90
- **Solver Name:** BoxModel
- **Required Output Variable(s):** Melt, Boxes
- **Required Input Variable(s):** Zb, distFL, distIF, Basin, SeaLevel, llGL, a, b, c, meltfac
- **Optional Output Variable(s):** None
- **Optional Input Variable(s):** None
- **Required Input File:** NetCDF with the parameters

## General Description

This solver is develop to compute the basal melt by using the Box Model (Reese, 2018). It will be used with the nearestpoint solver to have the basin variable. To compute the distance variable, the distance solver and FrontThickness_mask solver are used.

In the sif, many constant have to be defined. These constants aren’t specific to the box model

| Constant                                             | Keyword                 | Description                                       |
| ---------------------------------------------------- | ----------------------- | ------------------------------------------------- |
| $$z_{sl}$$                                           | sea level               | Sea level (m)                                     |
| $$cp_w$$                                             | Sea Water Specific heat | Sea Water Specific heat (J/kg/K)                  |
| Lf                                                   | Ice fusion latent heat  | Fusion Latent heat of Ice (J/kg)                  |
| $$\rho_i$$                                           | Ice density             | Ice density (kg/m^3)                              |
| $$\rho_{sw}$$                                        | water density           | Sea Water Density (kg/m^3)                        |
| a                                                    | Liquidus slope          | Salinity coefficient of freezing equation (K/psu) |
| b                                                    | Liquidus intercept      | Constant coefficient of freezing equation (K)     |
| c                                                    | Liquidus pressure coeff | Pressure coefficient of freezing equation (K/m)   |
| $$meltfac = \rho_{sw} * cp_{w} / ( \rho_{i} * Lf )$$ | Melt factor             |                                                   |

These constants have to be convert to the user units

In the NetCDF File, the solver will read the following variable and constant specific for the box model :

| Variable       | Keyword | Description                                       |
| -------------- | ------- | ------------------------------------------------- |
| $$S_0$$        |         | mean salinity (length : number of basin)          |
| $$T_0$$        |         | mean temperature (length : number of basin)       |
| $$n_{max}$$    |         | max number of boxes                               |
| C              |         | Circulation parameter                             |
| $$\gamma^*_T$$ |         | Effective turbulent temperature exchange velocity |
| $$\rho_*$$     |         | Reference density in EOS                          |
| $$\alpha$$     |         | Thermal expansion coefficient for linear EOS      |
| $$\beta$$      |         | Salinity contraction coefficient for linear EOS   |





- This solver first determine the **number of boxes $nD$ per basins** (b) by solving the equation:

$$n_D(b)= 1 + nearest\_integer(\sqrt{d_{GL}(D)/d_{max}}(n_{max}-1))$$ with $$d_{GL}(D)$$ the maximum distance within ice shelf $$D$$

and $$d_{max}$$ is the maximum distance to the grounding line within the entire computation domain

Define the box value of the node or element :

Relative distance to the GL : $$r = d_{GL}/(d_{GL}+d_{IF})$$

Box boundary : 

​	lower boundary : $$1.0-\sqrt{1.0*(nD-k+1)/n_{D}} $$ (for first boxes = 0)

​	higher boundary : $$ 1.0-\sqrt{1.0*(nD-k)/n_{D}} $$

- Then the solver compute the **total area of each box $$Abox$$**

- The goal is to solve the melt equation :

	- for the first boxes

		$$T_*=aS_0+b+c*z_b-T_0$$ with $z_b$ the depth of the element

		$$g1 = \gamma^*_T * A_{box}$$ 

		$$tmp1 =g1/(C*\rho_**(\beta*S_0*meltfac-\alpha)) $$

		$$xbox=0.5*tmp1+ \sqrt{(0.5*tmp1)^2-tmp1*T_*}$$

		TT = T_0-xbox

		$$T_1=T_1+TT*localunity$$

		$$SS=S_0-xbox*S_0*meltfac$$

		$$S_1=S_1+SS*localunity$$

		$$q = C \rho_*(\beta(S_{0}-S_{1})-\alpha(T_{0}-T_{1}))$$*localunity

		$$Melt = -\gamma^*_T*meltfac* (aSS+b+c*z_b-TT)$$

		

		For other boxes (from 2 to n)

		$$T_*=aS_{k-1}+b+c*z_b-T_{k-1}$$

		$$g1 = gT * A_{box}$$ 

		$$g2 = g1 * meltfac$$

		$$xbox=- g1 * T_* / ( q + g1 - g2*a*S_{k-1} )$$

		$$TT=T_{k-1}-xbox$$
		
		$$SS=S_{k-1}-xbox*S_{k-1}*meltfac$$
		
		$$T_k=T_k+TT*localunity$$
		
		$$S_k=S_k+SS*localunity$$
		
		$$Melt = -\gamma^*_T*meltfac* (aSS+b-c*z_b-TT)$$

## SIF contents

Solver section:

```sif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Constants
  sea level = Real $zsl
  water density = Real $rhow
  Sea Water Specific heat = Real $cw
  Ice fusion latent heat = Real $Lf
  Ice density = Real $rhoi
  Liquidus slope = Real $lambda1
  Liquidus intercept = Real $lambda2
  Liquidus pressure coeff = Real $lambda3
  Melt factor = Real $meltfac
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Grounding Line Melt = Logical False
  
  Coordinate System  = Cartesian 2D 
  Simulation Type = Steady State

  Output Intervals = 1

  Steady State Max Iterations = 1
  Steady State Min Iterations = 1

  Output File = "$name$.result"
  Post File = "$name$.vtu"

  Restart File = "TOPOGRAPHY.result"
  Restart Position = 0
  Restart Time = Real 0.0

  max output level = 20
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
 Bottom Surface Accumulation = Variable Melt
     REAL PROCEDURE "USFs_Ronnie_inversion" "MinusA"
  Water Pressure = Variable Zb
    Real procedure "USF_WaterPressure" "WaterPressure"
  distGL = Real 0.0  ! used in Solver DistanceSolver1
  distGL Condition = Variable GroundedMask
    Real procedure "USF_CondDistance" "CondDistance"
  distIF = Real 0.0
  distIF Condition = Variable FrontMask
    Real procedure "USF_CondFront" "CondFront"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Solver 1
  Equation = "distance GL"
  Variable = -dofs 1 distGL
    Procedure = "DistanceSolve" "DistanceSolver1"
  Optimize Bandwidth = logical False
End

Solver 2
  Equation = "FrontMask"
  Variable = FrontMask
  Variable DOFs = 1
    Procedure = "FrontThickness_mask" "FrontThickness_mask"
End

Solver 3
  Equation = "Real distance IF"
  Variable = -dofs 1 distIF
    Procedure = "DistanceSolve" "DistanceSolver1"
  Optimize Bandwidth = logical False
  Exported variable 1 = -dofs 1 distIF
End

Solver 4
   Equation = "point"
   Variable = -nooutput "dumy"

   procedure = "nearestpoint" "nearestpoint"
   Exported Variable 1 = -dofs 1 -elem basins
   !Exported Variable 1 = -dofs 1  basins

   Variable 1 = File "basins"
   Target Variable 1 = String "basins"
   Variable 1 data file = File "$DATA_DIR$/schmidtko_initmip8km_means.nc"
End

Solver 5
  Exported Variable 1 = -dofs 1 -elem Melt
  Exported Variable 2 = -dofs 1 -elem Boxes

   Equation = "box_model"
   Variable = -nooutput dummy
   Procedure = "box_model" "box_model"

   data file = File "$DATA_DIR$/DATA_Box_model.nc"

End

```

## Algorithm

msckrit is defined by the key word   Grounding Line Melt. If its value is “False” we applied a msckrit = -0.5 if its value is “True” we applied msckrit = 0.5.

### Definition of the number of boxes

basinmax(:) = 0 #vector length number of basins

distmax = 0 

boxes (:) = 0# nomber of boxes per basins

Loop on floating element:

​	compute the basinmax which is the max distance to the GL per basin

​	compute the distmax which is the max distance to the GL on the whole mesh

End loop

$$boxes = 1 + rd(\sqrt{basinlax/distmax}*(n_{max}-1))$$ 

### Computation of the box area and definition of the box number

Abox (:,:) =0.0#matrix number of basins X max number of boxes

rr(:) = 0 # vector to save relative distance to the ice front for each element

localunity (:) = 0.0 # vector to save area of each element

boxvalue(:) = 0

Loop on floating element e :

​	b <- basin value of the element

​	nD = boxes (b) # nombre of boxes for this basin b

​	$$rr(e) = \frac{\sum distGL(node)/ len(distGL(node)}{\sum distGL(node)/ len(distGL(node) + \sum distIF(node) / len(distGL(node))} $$ # relative distance to the Ice Front 

​	Loop on the gauusian integration point :

​		compute localunity

​	END Loop

​	Loop on the number of boxes kk=1,nD:

​		If $$1.0-\sqrt{1.0*(n_{max}-kk+1)/n_{max}}	<	rr	\leq	1.0-\sqrt{1.0*(n_{max}-kk)/n_{max}} $$ alors

​			Abox(kk,b) = Abox(kk,b) +localunity #aire des boites par bassins 

​			boxvalue(e)=kk

​		END IF

​	END Boucle

END Boucle

### Compute the melt for the first box

melt(:)=0.0 # number of elements

Tbox(:,:) =0.0

Sbox(:,:)=0.0

qqq(:)=0.0

Loop on the element where boxvalue > 0:

​		If boxvalue(e)==1 Then :

​	 	$$z_b$$ depth of the element

​		$$T_*=aS_0(bb)+b+c*z_b-T_0(bb)$$ # calcul T* difference entre la température de fonte et la temperature de la cavité

​		$$g1 = \gamma^*_T * A_{box}(1,bb)$$ # calcul du flux d’échange de la boite 1

​		$$tmp1 =g1/(C*\rho_**(\beta*S_0*meltfac-\alpha)) 	$$

​		$$xbox=0.5*tmp1+ \sqrt{(0.5*tmp1)^2-tmp1*T_*}$$

​		$$TT=T_0(bb)-xbox$$ # Calcul de l’évolution de la temperature entre la boite 0 et 1 pour un élément

​		$$SS=S_0(bb)-xbox*S_0(bb)*meltfac $$ # Calcul de l’évolution de la salinité entre la boite 0 et 1​ pour un élément

​		$$T(1,bb)=T(1,bb) + TT * localunity$$  # évolution de la température pondéré par l’aire de l’éléments

​		$$S(1,bb)=S(1,bb) + SS * localunity$$ # évolution de la salinité pondéré par l’aire de l’élémnts

​		$$qqq(bb) = qqq(bb) + C \rho_*(\beta(S_{0}(bb)-SS)-\alpha(T_{0}(bb)-TT)) * localunity$$ #overturning flux 

​		$$Melt(e) = -\gamma^*_T*meltfac* (aSS+b+c*z_b-TT)$$

​	END IF

END Loop

Loop on every basin to parallelize

Compute the mean of T(1,kk), S(1,kk), qqq(kk) per basins

### Compute of the melt for boxes 2 à $$n_{max}$$

Loop on other boxes kk = 2 à $$n_{max}$$:

Loop on the element where boxvalue > 0:

​		If boxvalue(e)==1 Then :

​					$$z_b$$ depth of the element

​				$$T_*=aS(kk-1,bb)+b+c*z_b-T_{k-1}$$

​				$$g1 = gT * A_{box}(kk,bb)$$ 

​				$$g2 = g1 * meltfac$$	

​				$$xbox=- g1 * T_* / ( q + g1 - g2*a*S_{k-1} )$$

​				$$TT = T(kk-1,bb)-xbox$$

​				$$SS = S(kk-1,bb)-xbox*S(kk-1,bb)*meltfac$$

​				$$T(kk,bb)=T(kk,bb)+TT * localunity$$ 

​				$$S(kk,bb)=S(kk,bb)+SS*localunity$$

​				$$Melt(e) = \gamma^*_T*meltfac* (aS_k+b-c*z_b-T_k)$$

​	END IF

END Loop

Loop on every basin to parallelize



