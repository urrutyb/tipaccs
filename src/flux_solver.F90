SUBROUTINE flux_solver( Model,Solver,dt,Transient )
!------------------------------------------------------------------------------
!USE CoordinateSystems
!USE MeshUtils
USE DefUtils
USE Netcdf


IMPLICIT NONE
!------------------------------------------------------------------------------
TYPE(Solver_t), TARGET :: Solver
TYPE(Model_t) :: Model
REAL(KIND=dp) :: dt
LOGICAL :: Transient

TYPE(ValueList_t), POINTER :: Params
TYPE(Variable_t), POINTER :: Var, VarC
TYPE(Nodes_t) :: ElementNodes
TYPE(Element_t),POINTER ::  Element
REAL(KIND=dp), POINTER :: VarValues(:), VarCValues(:)
INTEGER, POINTER :: VarPerm(:), VarCPerm(:), NodeIndexes(:)


CHARACTER(LEN=MAX_NAME_LEN) :: TargetVariableName,VariableName,DataF
CHARACTER(LEN=MAX_NAME_LEN) :: Xdim,dimName,FillName,Name,variabletype
CHARACTER(LEN=MAX_NAME_LEN) :: FName
CHARACTER(LEN=MAX_NAME_LEN),parameter :: &
                         SolverName='nearestpoint'
LOGICAL :: GotVar,GotTVar,Found,UnFoundFatal=.TRUE.
LOGICAL :: CheckBBox, NETCDFFormat
LOGICAL :: HaveFillv


INTEGER :: NetcdfStatus,varid,ncid
INTEGER :: NoVar,k,e
INTEGER :: nx,ny,n,node
INTEGER :: XMinIndex,XMaxIndex
INTEGER :: YMinIndex,YMaxIndex, Indexx, Indexy


REAL(KIND=dp) ::  Xb, Yb, dx, dy
REAL(KIND=DP),allocatable :: xx(:),yy(:),DEM(:,:)
REAL(KIND=DP) :: fillv
REAL(KIND=DP) :: Val
REAL(KIND=DP) :: xmin,xmax,ymin,ymax

CALL INFO(Trim(SolverName),'start solver', Level=5)

Params => GetSolverParams()

NoVar=0
GotVar=.True.

DO WHILE(GotVar)
   NoVar = NoVar + 1
   ! netcdf reading
   
   WRITE(Name,'(A,I0)') 'Variable ',NoVar

   Var => VariableGet( Model % Mesh % Variables, Name)
   IF (.NOT.ASSOCIATED(Var)) &
        &    CALL FATAL(SolverName,'variable not found')

   WRITE(Name,'(A,I0)') 'Variable Contour ',NoVar

   VarC => VariableGet( Model % Mesh % Variables, Name)
   IF (.NOT.ASSOCIATED(Var)) &
        &    CALL FATAL(SolverName,'variable contour not found')

   WRITE(Name,'(A,I0)') 'Variable output ',NoVar

   VarOut => VariableGet( Model % Mesh % Variables, Name)
   IF (.NOT.ASSOCIATED(MeltVar)) &
        &    CALL FATAL(SolverName,'Melt not found')
   
   Integ(:) = 0.0_dp
   DO e=1,Solver % NumberOfActiveElements
      Element => GetActiveElement(e)
      CALL GetElementNodes( ElementNodes )
      n = GetElementNOFNodes(Element)
      NodeIndexes => Element % NodeIndexes
      VisitedNode(NodeIndexes(1:n))=VisitedNode(NodeIndexes(1:n))+1.0_dp
      SELECT CASE(Melt % TYPE)
         CASE(Variable_on_elements)
            index = VarC(VarCPerm(e))
         CASE(Variable_on_nodes)
            index = VarC(VarCPerm(NodeIndexes(1:n)))
      localInteg = 0.0_dp
      IntegStuff = GaussPoints( Element )
      DO t=1,IntegStuff % n
         U = IntegStuff % u(t)
         V = IntegStuff % v(t)
         W = IntegStuff % w(t)
         stat = ElementInfo(Element,ElementNodes,U,V,W,SqrtElementMetric, &
              Basis,dBasisdx )
         s = SqrtElementMetric * IntegStuff % s(t)
         SELECT CASE(Melt % TYPE)
         CASE(Variable_on_elements)
            localInteg = localInteg + s * SUM(Basis(:) * Var(VarPerm(e)))
         CASE(Variable_on_nodes)
            localInteg = localInteg + s * SUM(Basis(:) * Var(VarPerm(NodeIndexes(:))))

      END DO
      Integ(index) = Integ + localInteg
   ENDDO

   DO e=1,Solver % NumberOfActiveElements
      Element => GetActiveElement(e)
      CALL GetElementNodes( ElementNodes )
      n = GetElementNOFNodes(Element)
      NodeIndexes => Element % NodeIndexes
      SELECT CASE(Melt % TYPE)
      CASE(Variable_on_elements)
         index = VarC(VarCPerm(e)) 
         VarOut(VarOutPerm(e))= Integ(index)
      CASE(Variable_on_nodes)
         index = VarC(VarCPerm(NodeIndexes(1:n)))
         VarOut(VarOutPerm(e))= Integ(index)   
end DO



  

CALL INFO(Trim(SolverName), &
     '-----ALL DONE----------',Level=5)

end SUBROUTINE flux_solver
